#!/usr/bin/env python

import argparse

from aiohttp import web

from app import create_app


args_parser = argparse.ArgumentParser()
args_parser.add_argument('--port', type=int, default=8080)
args_parser.add_argument('--host', default='localhost')
args = args_parser.parse_args()


web.run_app(create_app(), host=args.host, port=args.port)
