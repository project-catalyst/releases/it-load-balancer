#!/usr/bin/env python

import argparse
import asyncio
import colorsys
import json
import logging as log
import os
import sys
import urllib.parse

from aiohttp import web, ClientSession
from dataclasses import dataclass, field
from io import StringIO
from typing import Dict, List, Tuple

# Make itlb discoverable
sys.path.append('..')

# pylint: disable=import-error
import itlb.common.utils as utils

# pylint: disable=import-error
from itlb.server.clearing import ClearingRequest, ClearingResult, ClearingRequestItem


@dataclass
class ITLoad:
    cpu: int
    ram: int
    disk: int
    vc_tag: str
    orig_dc: str
    placed: bool


@dataclass
class Server:
    id: str
    cpu: int
    ram: int
    disk: int
    dc: str
    itloads: List[ITLoad] = field(default_factory=list)


@dataclass
class OfferingDC:
    id: str
    itloads: List[ITLoad] = field(default_factory=list)


@dataclass
class BiddingDC:
    id: str
    servers: List[Server] = field(default_factory=list)



class ViewGenerator:
    def __init__(self, request: ClearingRequest, result: ClearingResult, current_test: str):
        self.request = request
        self.result = result
        self.current_test = current_test

        if current_test:
            if not result:
                self.result_uri = '/result/{}'.format(current_test)
                self.request_uri = None
                self.offering_dcs, self.bidding_dcs = self._convert_request_data(self.request)
            else:
                self.request_uri = '/request/{}'.format(current_test)
                self.result_uri = None
                self.offering_dcs, self.bidding_dcs = self._convert_result_data(self.request, self.result)

            self._all_dcs = ([offering.id for offering in self.offering_dcs] +
                             [bidding.id for bidding in self.bidding_dcs])

            HLS_tuples = [(x*1.0/len(self.offering_dcs), 0.5, 0.8) for x in range(len(self.offering_dcs))]
            RGB_tuples = [colorsys.hls_to_rgb(*hls) for hls in HLS_tuples]
            scaled_RGB_tuples = [map(lambda x: int(x * 255), rgb) for rgb in RGB_tuples]
            self._dc_colors = {
                self.offering_dcs[i].id: '{:02x}{:02x}{:02x}'.format(*scaled_RGB_tuples[i])
                for i in range(len(self.offering_dcs))
            }
        else:
            self.result_uri = None
            self.request_uri = None
            self.offering_dcs = []
            self.bidding_dcs = []
            self._dc_colors = {}

        self._bids_grid_size = '6px'
        self._offers_grid_size = '6px'


    def build_html(self):
        sb = StringIO()
        self._write_header(sb)
        self._write_body(sb)

        return sb.getvalue()


    def _write_header(self, sb):
        sb.write('''
            <html lang="en">
            <head>
              <meta charset="utf-8">

              <title>ITLB visualisation</title>
              <meta name="description" content="Layout test">
              <meta name="author" content="Ashamed Anonymous">

              <style>''')
        self._write_css(sb)
        sb.write('</style></head>')

    def _write_css(self, sb):
        if not self.offering_dcs or not self.bidding_dcs:
            max_cpu = 1
            max_ram = 1
        else:
            max_cpu = max(max(itl.cpu for dc in self.offering_dcs for itl in dc.itloads),
                          max(srv.cpu for dc in self.bidding_dcs for srv in dc.servers))

            max_ram = max(max(itl.ram for dc in self.offering_dcs for itl in dc.itloads),
                          max(srv.ram for dc in self.bidding_dcs for srv in dc.servers))


        sb.write(f'''
            body {{
                font-family: sans;
            }}

            #top {{
                display: grid;
                grid-template-columns: repeat(3, 1fr);
            }}

            .step_link  a:visited, .step_link a {{
                font-size: 3em;
                color: #23b572;
                text-decoration: none;
            }}

            .step_link  a:visited, .step_link a {{
                font-size: 3em;
                color: #009245;
                text-decoration: none;
            }}

            .tests_links  a:visited, .tests_links a {{
                font-size: 2em;
                color: #23b572;
                text-decoration: none;
            }}

            .current_test a:visited, .current_test a {{
                font-size: 2em;
                color: #adaf1a;
                text-decoration: none;
            }}

            h1 {{
                text-align: center;
                padding: 30px;
            }}

            #legend  {{
                padding-left: 50px;
                padding-bottom: 20px;
            }}

            h3 {{
                color: rgb(35, 181, 114);
            }}

            .rightarrow::before {{
                width: 300px;
                content: "\\27F6";
            }}

            .all {{
                display: grid;
                grid-template-columns: 1fr 2fr;
                grid-gap: 70px;
            }}

            .offers {{
                grid-column-start: 1;
            }}

            .bids {{
                grid-column-start: 2;
            }}

            .itloads {{
                display: grid;
                grid-template-columns: repeat(64, {self._offers_grid_size});
                grid-gap: 2px;
                grid-auto-rows: minmax({self._offers_grid_size}, {self._offers_grid_size});
            }}

            .servers {{
                display: grid;
                grid-template-columns: repeat(200, {self._bids_grid_size});
                grid-gap: 0px;
                grid-auto-rows: minmax({self._bids_grid_size}, {self._bids_grid_size});
            }}

            .slot {{
                border: 1px solid;
            }}

            h2 {{
                float: left;
            }}

            ul {{
                list-style-type: none;
            }}
        ''')

        for i in range(1, max_cpu + 1):
            sb.write('.cpu{i} {{ grid-column-end: span {i}; }}\n'.format(i=i))

        for i in range(1, max_ram + 1):
            sb.write('.ram{i} {{ grid-row-end: span {i}; }}\n'.format(i=i))

        for dc_id, dc_color in self._dc_colors.items():
            sb.write(f'.{dc_id}_itl {{ background: #{dc_color}; }}')
            sb.write(f'.{dc_id}_slot {{ background: #{dc_color}; }}')
            sb.write(f'.{dc_id}_placed {{ background: repeating-linear-gradient(45deg, #{dc_color}, #{dc_color} 2px, #ffffff 0px, #ffffff 6px); }}')



    def _write_body(self, sb):
        sb.write('<body>')
        self._write_top(sb)
        sb.write('<hr />')
        if self.current_test:
            sb.write('<div class="all">')
            self._write_offers(sb)
            self._write_bids(sb)
            sb.write('</div>')

        sb.write('''
            </body>
        </html>''')



    def _write_top(self, sb):
        sb.write('''
                <h1>IT load balancer demo</h1>
                <div id="top">
                    <div id="legend">
                        <img src="/static/cpu-ram-arrows.png" width=300px />
                    </div>
        ''')

        if self.request_uri:
            sb.write(f'<p class="step_link"><a href={self.request_uri}>View request</a></p>')
        if self.result_uri:
            sb.write(f'<p class="step_link"><a href={self.result_uri}>View result</a></p>')

        sb.write('<ul class="tests_links">')
        for filename in sorted(os.listdir('sample/input')):
            path = os.path.join('sample/input', filename)
            if os.path.isfile(path):
                test_name = '.'.join(filename.split('.')[:-1])
                if test_name == self.current_test:
                    sb.write(f'<li class="current_test"><a href="/request/{test_name}">{test_name}</a></li>')
                else:
                    sb.write(f'<li><a href="/request/{test_name}">{test_name}</a></li>')
        sb.write('</ul>')



        sb.write('</div>')




    def _write_offers(self, sb):
        sb.write('<div class="offers">')
        for offering_dc in self.offering_dcs:
            sb.write('<div class="offering_dc">')
            sb.write(f'<h3>{offering_dc.id}</h3>')
            sb.write(f'<div class="itloads {offering_dc.id}">')
            for itload in offering_dc.itloads:
                placed_class = f'{offering_dc.id}_placed' if itload.placed else ''
                sb.write(f'<div class="slot {offering_dc.id}_slot cpu{itload.cpu} ram{itload.ram} {placed_class}"></div>')
            sb.write('</div>')  # itloads
            sb.write('</div>')  # offering_dc
        sb.write('</div>')  # offers


    def _write_bids(self, sb):
        sb.write('<div class="bids">')
        for bidding_dc in self.bidding_dcs:
            sb.write('<div class="bidding_dcs">')
            sb.write(f'<h3>{bidding_dc.id}</h3>')
            sb.write(f'<div class="servers {bidding_dc.id}">')
            for server in bidding_dc.servers:
                style = f'''
                    display: grid;
                    grid-template-columns: repeat({server.cpu}, {self._bids_grid_size});
                    grid-gap: 0px;
                    grid-template-rows: repeat({server.ram}, {self._bids_grid_size});'''
                sb.write(f'<div style="{style}" class="slot cpu{server.cpu} ram{server.ram}">')

                cur_cpu, cur_ram = 1, 1
                for itload in server.itloads:
                    style = (f'grid-column-start: {cur_cpu}; grid-column-end: {cur_cpu + itload.cpu};' +
                             f'grid-row-start: {cur_ram}; grid-row-end: {cur_ram + itload.ram};')
                    sb.write(f'<div style="{style}" class="slot {itload.orig_dc}_itl"></div>')
                    cur_cpu += itload.cpu
                    cur_ram += itload.ram
                sb.write('</div>')
            sb.write('</div>')  # servers
            sb.write('</div>')  # offering_dc
        sb.write('</div>')  # offers



    def _convert_request_data(self, clearing_request: ClearingRequest) -> Tuple[List[OfferingDC],
                                                                                List[BiddingDC]]:
        itloads: Dict[str, OfferingDC] = {}
        for offer in clearing_request.offers:
            itls_for_dc = itloads.get(offer.dc_id)
            if not itls_for_dc:
                itls_for_dc = OfferingDC(offer.dc_id)
                itloads[offer.dc_id] = itls_for_dc
            itl = self._offer_to_itl(offer, False)
            itls_for_dc.itloads.append(itl)

        servers: Dict[str, BiddingDC] = {}
        for i, bid in enumerate(clearing_request.bids):
            servers_for_dc = servers.get(bid.dc_id)
            if not servers_for_dc:
                servers_for_dc = BiddingDC(bid.dc_id)
                servers[bid.dc_id] = servers_for_dc
            server = self._bid_to_server(i, bid)
            servers_for_dc.servers.append(server)

        return list(itloads.values()), list(servers.values())



    def _convert_result_data(self, clearing_request: ClearingRequest, clearing_result: ClearingResult) -> \
                             Tuple[List[OfferingDC], List[BiddingDC]]:

        def is_placed(itload):
            return not any(rejected.vc_tag == itload.vc_tag for rejected in clearing_result.rejected)

        itloads: Dict[str, OfferingDC] = {}
        for offer in clearing_request.offers:
            itls_for_dc = itloads.get(offer.dc_id)
            if not itls_for_dc:
                itls_for_dc = OfferingDC(offer.dc_id)
                itloads[offer.dc_id] = itls_for_dc
            itl = self._offer_to_itl(offer, is_placed(offer))
            itls_for_dc.itloads.append(itl)

        servers: Dict[str, BiddingDC] = {}
        for i, bid in enumerate(clearing_request.bids):
            servers_for_dc = servers.get(bid.dc_id)
            if not servers_for_dc:
                servers_for_dc = BiddingDC(bid.dc_id)
                servers[bid.dc_id] = servers_for_dc
            server = self._bid_to_server(i, bid)

            accepted_offers = [itload
                               for accepted in clearing_result.accepted
                               if accepted.dc_id == bid.dc_id
                               for itload in accepted.offers
                               if itload.target_server == server.id]
            log.error("Accepted offers for %s: %d", server.id, len(accepted_offers))
            for accepted in clearing_result.accepted:
                if not accepted.dc_id == bid.dc_id:
                    continue

            server.itloads = [self._offer_to_itl(item, False) for item in accepted_offers]
            servers_for_dc.servers.append(server)

        return list(itloads.values()), list(servers.values())


    def _offer_to_itl(self, offer, is_placed):
        cpu, ram, disk = 0, 0, 0
        for load_value in offer.load_values:
            if load_value.parameter == 'cpu':
                cpu = load_value.value
            elif load_value.parameter == 'ram':
                ram = utils.to_mega_bytes(load_value.value, load_value.uom) // 1024
            elif load_value.parameter == 'disk':
                disk = utils.to_mega_bytes(load_value.value, load_value.uom) // 1024

        return ITLoad(cpu, ram, disk, offer.vc_tag, offer.dc_id, is_placed)


    def _bid_to_server(self, i: int, bid: ClearingRequestItem):
        cpu, ram, disk = 0, 0, 0
        for load_value in bid.load_values:
            if load_value.parameter == 'cpu':
                cpu = load_value.value
            elif load_value.parameter == 'ram':
                ram = utils.to_mega_bytes(load_value.value, load_value.uom) // 1024
            elif load_value.parameter == 'disk':
                disk = utils.to_mega_bytes(load_value.value, load_value.uom) // 1024

        return Server(f'{bid.dc_id}_{i}', cpu, ram, disk, bid.dc_id)













class MarketplaceMock:
    def __init__(self, config: dict):
        self._config = config
        self._lock = asyncio.Lock()
        self._pending_clearings: Dict[str, Tuple[ClearingRequest, asyncio.Future]] = {}
        self._http_client_session = ClientSession()

        self.app = web.Application(
            middlewares=[
                MarketplaceMock._catch_json_decode_error,
            ],
            client_max_size=1024 * 1024 * 100
        )

        self.app.add_routes([
            web.get("/", self._index),
            web.get("/index", self._index),
            web.get("/request/{name}", self._request),
            web.get("/static-result/{name}", self._static_result),
            web.get("/result/{name}", self._result),
            web.post("/clearingResult", self._clearing_result),
        ])

        self.app.router.add_static('/static', './static')




    @staticmethod
    @web.middleware
    async def _catch_json_decode_error(request, handler):
        try:
            return await handler(request)
        except json.decoder.JSONDecodeError:
            raise web.HTTPBadRequest(body=json.dumps({'message': "Could not decode json body"}))


    async def _index(self, _):
        generator = ViewGenerator(None, None, None)
        html = generator.build_html()
        return web.Response(text=html, content_type='text/html')

    async def _request(self, request):
        test_name = urllib.parse.unquote(request.match_info['name'])

        if test_name:
            with open(f'sample/input/{test_name}.json') as request_file:
                clearing_request_data = json.load(request_file)
            clearing_request = ClearingRequest.Schema().load(clearing_request_data)
            generator = ViewGenerator(clearing_request, None, current_test=test_name)
            html = generator.build_html()
        else:
            generator = ViewGenerator(None, None, None)
            html = generator.build_html()
        return web.Response(text=html, content_type='text/html')


    async def _result(self, request):
        test_name = urllib.parse.unquote(request.match_info['name'])
        log.info("Start clearing for %s", test_name)
        with open(f'sample/input/{test_name}.json') as request_file:
            clearing_request_data = json.load(request_file)
        clearing_request = ClearingRequest.Schema().load(clearing_request_data)

        log.info("Clearing for %s: create and register future", test_name)
        loop = asyncio.get_event_loop()
        future_result = loop.create_future()
        async with self._lock:
            self._pending_clearings[clearing_request.id] = (future_result, clearing_request)

        log.info("Clearing for %s: post request to ITLB server", test_name)
        await self._post_clearing_request(clearing_request)

        log.info("Clearing for %s: wait for result", test_name)
        clearing_request, clearing_result = await future_result

        log.info("Clearing for %s: got result, render and return", test_name)
        generator = ViewGenerator(clearing_request, clearing_result, current_test=test_name)
        html = generator.build_html()
        return web.Response(text=html, content_type='text/html')


    async def _post_clearing_request(self, clearing_request):
        uri = urllib.parse.urljoin(self._config.itlb_address, 'v1/clearingRequest')
        clearing_request_data = ClearingRequest.Schema().dump(clearing_request)

        await self._http_client_session.post(uri, json=clearing_request_data )


    async def _clearing_result(self, request):
        clearing_result_data = await request.json()
        clearing_result = ClearingResult.Schema().load(clearing_result_data)
        async with self._lock:
            future, clearing_request = self._pending_clearings.get(clearing_result.id)
            future.set_result((clearing_request, clearing_result))
        return web.Response()


    # For debug only
    async def _static_result(self, request):
        test_name = urllib.parse.unquote(request.match_info['name'])

        with open(f'sample/{test_name}.json') as request_file:
            clearing_request_data = json.load(request_file)
        clearing_request = ClearingRequest.Schema().load(clearing_request_data)

        with open(f'sample/{test_name}-result.json') as request_file:
            clearing_result_data = json.load(request_file)
        clearing_result = ClearingResult.Schema().load(clearing_result_data)

        generator = ViewGenerator(clearing_request, clearing_result, current_test=test_name)
        html = generator.build_html()
        return web.Response(text=html, content_type='text/html')






def create_app():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--listen-address', default='127.0.0.1:6001')
    args_parser.add_argument('-l', '--log-level', default='INFO', choices=['DEBUG', 'INFO', 'WARN', 'ERROR'])
    args_parser.add_argument('--itlb-address', default='http://127.0.0.1:6000')
    args, _ = args_parser.parse_known_args()

    log.basicConfig(level=args.log_level,
                    format="%(asctime)s|%(filename)s:%(lineno)s|%(funcName)s|%(levelname)s|%(message)s",
                    stream=sys.stderr)

    mock = MarketplaceMock(args)

    return mock.app
