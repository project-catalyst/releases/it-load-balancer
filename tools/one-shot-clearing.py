#!/usr/bin/env python

import argparse
import json
import logging as log
import sys

sys.path.append('../..')

from itlb.server.clearing import ClearingResult, ClearingRequest, OfferValueComputerCpuRam  # noqa: E402
from itlb.server.clearing_fte import ClearingStrategyFollowTheEnergy  # noqa: E402


args_parser = argparse.ArgumentParser()
args_parser.add_argument("--input", "-i", help="Input data file", required=True)
args_parser.add_argument("--log-level", "-l")

args = args_parser.parse_args()

log.basicConfig(level=args.log_level,
                format="%(asctime)s|%(filename)s:%(lineno)s|%(funcName)s|%(levelname)s|%(message)s",
                stream=sys.stderr)

with open(args.input) as input_file:
    clearing_request_tree = json.load(input_file)
    clearing_request = ClearingRequest.Schema().load(clearing_request_tree)


clearing = ClearingStrategyFollowTheEnergy(OfferValueComputerCpuRam())

clearing_result = clearing.clear_marketplace_round(clearing_request)
clearing_result_tree = ClearingResult.Schema().dump(clearing_result)
