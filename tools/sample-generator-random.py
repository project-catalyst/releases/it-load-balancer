#!/usr/bin/env python3


import argparse
import json
import random


args_parser = argparse.ArgumentParser()
args_parser.add_argument('-i', '--input', required=True)
args_parser.add_argument('-o', '--output', required=True)
args = args_parser.parse_args()


with open(args.input) as input_file:
    spec = json.load(input_file)


offers = []
for dc_id, dc_spec in spec["offering"].items():
    for i in range(dc_spec['count']):
        offers.append({
            "date": spec['date'],
            "id": i,
            "dc_id": dc_id,
            "starttime": spec['starttime'],
            "endtime": spec['endtime'],
            "price": 0,
            "action_type": "offer",
            "load_values": [
                {
                    "parameter": "cpu",
                    "value": random.randint(dc_spec['cpu']['min'], dc_spec['cpu']['max']),
                    "uom": "cpu"
                },
                {
                    "parameter": "ram",
                    "value": 1024 * random.randint(dc_spec['ram']['min'], dc_spec['ram']['max']),
                    "uom": "MB"
                },
                {
                    "parameter": "disk",
                    "value": dc_spec['disk'],
                    "uom": "GB"
                }
            ]
        })


bids = []
for dc_id, dc_spec in spec["bidding"].items():
    for i in range(dc_spec['count']):
        bids.append({
            "date": spec['date'],
            "id": i,
            "dc_id": dc_id,
            "starttime": spec['starttime'],
            "endtime": spec['endtime'],
            "price": 0,
            "action_type": "bid",
            "load_values": [
                {
                    "parameter": "cpu",
                    "value": random.randint(dc_spec['cpu']['min'], dc_spec['cpu']['max']),
                    "uom": "cpu"
                },
                {
                    "parameter": "ram",
                    "value": 1024 * random.randint(dc_spec['ram']['min'], dc_spec['ram']['max']),
                    "uom": "MB"
                },
                {
                    "parameter": "disk",
                    "value": dc_spec['disk'],
                    "uom": "GB"
                }
            ]
        })


result = {
    "id": 1,
    "offers": offers,
    "bids": bids
}


with open(args.output, 'w+') as output_file:
    json.dump(result, output_file, indent=4)
