import argparse
import asyncio
import logging as log
import sys
import yaml

from .clearing import ClearingStrategyNothing, OfferValueComputerCpuRam, OfferValueComputerExternalPriority
from .clearing_fte import ClearingStrategyFollowTheEnergy
from .loadbalancer import LoadBalancerDaemon
from .marketplace import MarketplaceClientFile, MarketplaceClientHttp, MarketplaceEndpoint
from .persist import MemoryPersistor


def value_computer_builder(config):
    value_computer_name = config.get('itl-value-computer')
    if not value_computer_name or value_computer_name == 'external-priority':
        return OfferValueComputerExternalPriority()
    elif value_computer_name == 'cpu-ram':
        return OfferValueComputerCpuRam()
    else:
        raise ValueError("Unkwnown itl-value-computer '{}'".format(value_computer_name))


def strategy_builder(config):
    strategy_name = config.get('strategy')
    if not strategy_name:
        raise ValueError("Missing configuration option 'strategy'")

    value_computer = value_computer_builder(config)

    if strategy_name == 'nothing':
        return ClearingStrategyNothing(value_computer)
    elif strategy_name == 'follow-the-energy':
        return ClearingStrategyFollowTheEnergy(value_computer, shuffle=False)
    elif strategy_name == 'follow-the-energy-shuffle':
        return ClearingStrategyFollowTheEnergy(value_computer, shuffle=True)
    else:
        raise ValueError("Unkwnown strategy '{}'".format(strategy_name))


async def main():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--config", required=True, help="Configuration file")
    args = args_parser.parse_args()

    try:
        with open(args.config) as config_file:
            config = yaml.safe_load(config_file)
    except Exception:
        log.exception("Cannot parse configuration file %s", args.config)
        sys.exit(1)

    mp_config = config.get('marketplace-client')
    if mp_config and mp_config['type'] == 'http':
        if not mp_config['config'] or not mp_config['config']['remote_endpoint']:
            raise KeyError("Missing 'remote_endpoint' configuration for 'http' marketplace client")
        mp_client = MarketplaceClientHttp(**mp_config['config'])
    else:
        mp_client = MarketplaceClientFile()
    persistor = MemoryPersistor()
    clearing_strategy = strategy_builder(config)
    lb_daemon = LoadBalancerDaemon(config, mp_client, persistor, clearing_strategy)
    mp_endpoint = MarketplaceEndpoint(config, lb_daemon)

    log.basicConfig(level=config['log_level'],
                    format="%(asctime)s|%(filename)s:%(lineno)s|%(funcName)s|%(levelname)s|%(message)s",
                    stream=sys.stderr)

    await mp_endpoint.start()
    lb_daemon_task = lb_daemon.start()

    await lb_daemon_task


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        log.warning("Exiting as requested")
    except Exception as ex:
        log.exception("Exception in main application, of type %s", type(ex))
