import itertools
import functools
import operator
import time

from abc import ABC, abstractmethod
from datetime import datetime
from marshmallow import Schema as MSchema
from typing import ClassVar, List, Optional, Type, TYPE_CHECKING

if TYPE_CHECKING:
    from dataclasses import dataclass
else:
    from marshmallow_dataclass import dataclass

from ..common.utils import jsonify, to_mega_bytes, WithSchema


##########
# Those classes are there for exchange with the outside,
# not necessarily correclty organized for direct use by
# strategies.
##########

@dataclass
class LoadValue(WithSchema):
    ''' A generic key - value - unit of measurement container '''
    parameter: str
    value: int
    uom: str
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@dataclass
class ClearingRequestItem(WithSchema):
    ''' Item of a clearing request: either an offer or a bid '''
    id: int
    date: datetime
    dc_id: str
    starttime: datetime
    endtime: datetime
    price: int
    action_type: str
    load_values: List[LoadValue]
    target_server: Optional[str]
    priority: Optional[int] = 0
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    def get_cpu_ram_disk(self):
        cpu, ram, disk = 0, 0, 0
        for load_value in self.load_values:
            if load_value.parameter == 'cpu':
                cpu = load_value.value
            elif load_value.parameter == 'ram':
                ram = to_mega_bytes(load_value.value, load_value.uom)
            elif load_value.parameter == 'disk':
                disk = to_mega_bytes(load_value.value, load_value.uom)

        return cpu, ram, disk


# Translation class between the previously thought ClearingRequestItem and the
# version that will actually be used by the Marketplace. We do this translation
# so we don't have to change all the rest of the code.
@dataclass
class ClearingRequestItemFromMarketplace(WithSchema):
    id: int
    date: datetime
    actionStartTime: datetime
    actionEndTime: datetime
    price: float
    marketActorid: int
    marketSessionid: int
    actionTypeid: int
    cpu: int
    ram: int
    disk: int
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    class Meta:
        unknown = 'exclude'


@dataclass
class ClearingRequestItemFromMarketplaceV2(WithSchema):
    marketaction: ClearingRequestItemFromMarketplace
    priority: int
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    class Meta:
        unknown = 'exclude'


@dataclass
class ClearingRequest(WithSchema):
    ''' Full clearing request, which processing will produce a
        ClearingResult '''
    id: int
    offers: List[ClearingRequestItem]
    bids: List[ClearingRequestItem]
    orig_request: List[ClearingRequestItemFromMarketplace] = None
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    class Meta:
        unknown = 'exclude'

    def short_str(self):
        offers_by_dc = itertools.groupby(sorted(self.offers, key=lambda of: of.dc_id),
                                         key=lambda of: of.dc_id)
        bids_by_dc = itertools.groupby(sorted(self.bids, key=lambda of: of.dc_id),
                                       key=lambda of: of.dc_id)


        return jsonify(
            {
                'offers': {
                    dc_id: functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                            map(operator.methodcaller('get_cpu_ram_disk'), itloads),
                                            (0, 0, 0))
                    for dc_id, itloads in offers_by_dc
                },
                'bids': {
                    dc_id: functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                            map(operator.methodcaller('get_cpu_ram_disk'), servers),
                                            (0, 0, 0))
                    for dc_id, servers in bids_by_dc
                },
            }
        )



@dataclass
class AcceptedOffers(WithSchema):
    dc_id: str
    offers: List[ClearingRequestItem]
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@dataclass
class ClearingResult(WithSchema):
    id: int
    accepted: List[AcceptedOffers]
    rejected: List[ClearingRequestItem]
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    def short_str(self):
        return jsonify(
            {
                'accepted': {
                    accepted.dc_id: functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                                     map(operator.methodcaller('get_cpu_ram_disk'), accepted.offers),
                                                     (0, 0, 0))
                    for accepted in self.accepted
                },
                'rejected': functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                             map(operator.methodcaller('get_cpu_ram_disk'), self.rejected),
                                             (0, 0, 0))
            },
            indent=4)



@dataclass
class ClearingStats:

    def __init__(self, request: ClearingRequest, results: ClearingResult):
        self.request_id = request.id

        self.total_vcpus: int = 0
        self.total_vram: int = 0
        self.total_vdisk: int = 0

        self.total_hw_cpus: int = 0
        self.total_hw_ram: int = 0
        self.total_hw_disk: int = 0
        self.total_offering_dcs: int = 0
        self.total_bidding_dcs: int = 0

        self.placed_vcpus: int = 0
        self.placed_vram: int = 0
        self.placed_vdisk: int = 0
        self.placed_offering_dcs: int = 0
        self.used_bidding_dcs: int = 0

        bidding_dcs = set()
        offering_dcs = set()

        for offer in request.offers:
            cpu, ram, disk = offer.get_cpu_ram_disk()
            self.total_vcpus += cpu
            self.total_vram += ram
            self.total_vdisk += disk
            offering_dcs.add(offer.dc_id)
        self.total_offering_dcs = len(offering_dcs)

        for bid in request.bids:
            cpu, ram, disk = bid.get_cpu_ram_disk()
            self.total_hw_cpus += cpu
            self.total_hw_ram += ram
            self.total_hw_disk += disk
            bidding_dcs.add(bid.dc_id)
        self.total_bidding_dcs = len(bidding_dcs)

        used_bidding_dcs = set()
        placed_offering_dcs = set()


        for accepted_offer in results.accepted:
            for item in accepted_offer.offers:
                cpu, ram, disk = item.get_cpu_ram_disk()
                self.placed_vcpus += cpu
                self.placed_vram += ram
                self.placed_vdisk += disk
                placed_offering_dcs.add(item.dc_id)
            used_bidding_dcs.add(accepted_offer.dc_id)



        self.used_bidding_dcs = len(used_bidding_dcs)
        self.placed_offering_dcs = len(placed_offering_dcs)

        # Back to GB for disk
        self.total_vdisk //= 1024
        self.total_hw_disk //= 1024
        self.placed_vdisk //= 1024

    def __str__(self):
        return (f'placed:\n' +
                f'\t{self.placed_vcpus} CPUs over {self.total_vcpus} CPUs offered ' +
                f'on {self.total_hw_cpus} bid CPUs\n' +
                f'\t{self.placed_vram} MB RAM over {self.total_vram} MB RAM offered ' +
                f'on {self.total_hw_ram} MB bid RAM\n' +
                f'\t{self.placed_vdisk} GB disk over {self.total_vdisk} GB disk offered ' +
                f'on {self.total_hw_disk} GB bid disk, ' +
                f'fullfilling offers of {self.placed_offering_dcs} / {self.total_offering_dcs} offering DCs, ' +
                f'spread over {self.used_bidding_dcs} / {self.total_bidding_dcs} bidding DCs')




class IOfferValueComputer(ABC):
    @abstractmethod
    def compute_value(self, offer: ClearingRequestItem):
        pass


class OfferValueComputerCpuRam(IOfferValueComputer):
    def compute_value(self, offer: ClearingRequestItem):
        # After all why not. Anyway this'll do for development
        cpu, ram, _ = offer.get_cpu_ram_disk()
        return cpu * ram


class OfferValueComputerExternalPriority(IOfferValueComputer):
    def compute_value(self, offer: ClearingRequestItem):
        return offer.priority


class IClearingStrategy(ABC):
    def __init__(self, itload_value_computer: IOfferValueComputer):
        self._value_computer = itload_value_computer

    @abstractmethod
    def clear_marketplace_round(self, clearing_request: ClearingRequest) -> ClearingResult:
        pass


class ClearingStrategyNothing(IClearingStrategy):
    def clear_marketplace_round(self, clearing_request: ClearingRequest) -> ClearingResult:
        time.sleep(2)
        return ClearingResult(id=clearing_request.id, accepted=[], rejected=clearing_request.offers)
