import asyncio
import contextlib
import json
import logging as log
import marshmallow
import random
import urllib.parse

from abc import ABC, abstractmethod
from aiohttp import web, ClientSession
from aiohttp_swagger import setup_swagger
from datetime import datetime
from http import HTTPStatus
from marshmallow import Schema as MSchema
from urllib.parse import urljoin
from typing import ClassVar, Dict, List, Type, TYPE_CHECKING

if TYPE_CHECKING:
    from dataclasses import dataclass
else:
    from marshmallow_dataclass import dataclass

from .clearing import ClearingResult, ClearingRequest, ClearingRequestItem, ClearingRequestItemFromMarketplace, ClearingRequestItemFromMarketplaceV2, LoadValue
from .clearing_fte import Server, ITLoad
from ..common.utils import json_response, jsonify, WithSchema
from .demo_ui import ViewGenerator



@dataclass
class MatchedBidsOffers(WithSchema):
    marketAction_Bid_id: int
    marketAction_Offer_id: int
    exchangedValue: int = 1
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy



class IMarketplaceClient(ABC):
    @abstractmethod
    async def report_clearing_result(self, clearing_request: ClearingRequest,
                                     clearing_result: ClearingResult, raw_placement) -> None:
        pass


class MarketplaceClientFile(IMarketplaceClient):
    async def report_clearing_result(self, clearing_request: ClearingRequest,
                                     clearing_result: ClearingResult, raw_placement) -> None:
        with open('temp/{}.json'.format(clearing_result.id), 'w+') as out_file:
            out_file.write(jsonify(clearing_result))


class MarketplaceHttp:
    def __init__(self, remote_endpoint, user, password):
        self._remote_endpoint = remote_endpoint
        self._user = user
        self._password = password  # Pretty good practice keeping this in the clear in a variable, sorry.
        self._token = None

    async def _get_token(self):
        if self._token is None:
            log.warning("Authenticating to the marketplace for user %s", self._user)
            self._token = await self._authenticate()
        return self._token


    async def _authenticate(self):
        async with ClientSession() as session:
            url = urljoin(self._remote_endpoint, 'tokens/')
            resp = await session.post(url=url, json={"username": self._user, "password": self._password})
            resp.raise_for_status()
            resp_json = await resp.json()
            return resp_json['token']

    @contextlib.asynccontextmanager
    async def _session(self):
        token = await self._get_token()
        client_session = ClientSession(headers={"Authorization": f"token {token}"})
        try:
            yield client_session
        finally:
            await client_session.close()


class MarketplaceClientHttp(IMarketplaceClient, MarketplaceHttp):
    def __init__(self, *, remote_endpoint, user, password):
        MarketplaceHttp.__init__(self, remote_endpoint, user, password)

    @classmethod
    def _raw_placement_to_matched_bids_offers(cls, raw_placement):
        all_matches = []
        for server, it_loads in raw_placement.items():
            for itl in it_loads:
                match = MatchedBidsOffers(marketAction_Bid_id=server.original_item.id,
                                          marketAction_Offer_id=itl.original_item.id)
                all_matches.append(match)
        return all_matches


    @classmethod
    def _get_all_market_action_ids(cls, clearing_request: ClearingRequest) -> List[int]:
        actions = []
        for offer in clearing_request.offers:
            actions.append(offer.id)
        for bid in clearing_request.bids:
            actions.append(bid.id)

        return actions


    @classmethod
    def create_clearing_results_response(cls, clearing_request: ClearingRequest,
                                          raw_placement: Dict[Server, List[ITLoad]]):
        all_market_actions = cls._get_all_market_action_ids(clearing_request)
        matched_bids_offers = cls._raw_placement_to_matched_bids_offers(raw_placement)
        json_matched_bids_offers = {
            'counteroffers': [MatchedBidsOffers.Schema().dump(match) for match in matched_bids_offers],
            'actions': all_market_actions,
        }
        return json_matched_bids_offers



    async def report_clearing_result(self, clearing_request: ClearingRequest,
                                     clearing_result: ClearingResult, raw_placement) -> None:
        async with self._session() as session:
            json_matched_bids_offers = self.create_clearing_results_response(clearing_request, raw_placement)
            # clearing_result_data = ClearingResult.Schema().dump(json_ma)
            url = urljoin(self._remote_endpoint, "marketactioncounteroffers/it-clearing/")
            await session.post(url=url, json=json_matched_bids_offers)


class MarketplaceEndpoint:
    def __init__(self, config: dict, load_balancer):
        self._config = config
        self._load_balancer = load_balancer
        self._listen_address = urllib.parse.urlparse("http://" + self._config['listen_address'])
        if self._config['id-translator']['type'] == 'fake':
            self._id_translator = DASMarketplaceNumericIdToSemanticMapperFake()
        else:
            mp_endpoint = self._config['marketplace-client']['config']['remote_endpoint']
            mp_user = self._config['marketplace-client']['config']['user']
            mp_password = self._config['marketplace-client']['config']['password']
            self._id_translator = DASMarketplaceNumericIdToSemanticMapperHttp(
                remote_endpoint=mp_endpoint, user=mp_user, password=mp_password)

        self._app = web.Application(
            middlewares=[
                MarketplaceEndpoint._catch_json_decode_error,
                MarketplaceEndpoint._catch_marshmallow_error,
            ],
            client_max_size=1024 * 1024 * 100
        )

        self._app.add_routes([
            web.post("/clearingRequest", self._post_clearing_request),
            web.post("/clearingRequest/", self._post_clearing_request),
            web.post("/v1/clearingRequest", self._post_clearing_request),
            web.post("/v1/clearingRequest/", self._post_clearing_request),
            web.post("//v1/clearingRequest", self._post_clearing_request),
            web.post("//v1/clearingRequest/", self._post_clearing_request),
            web.post("/v2/clearingRequest", self._post_clearing_request_v2),
            web.post("/v2/clearingRequest/", self._post_clearing_request_v2),
            web.post("//v2/clearingRequest", self._post_clearing_request_v2),
            web.post("//v2/clearingRequest/", self._post_clearing_request_v2),
            web.get("/demo/history", self._get_all_past_clearings),
            web.get("/demo/view/{clearing_id}/request", self._get_clearing_request_view),
            web.get("/demo/view/{clearing_id}/result", self._get_clearing_result_view),
        ])

        # Static content for demo pages
        self._app.router.add_static('/demo/static', './itlb/server/demo/static')

        setup_swagger(self._app)

    @property
    def _listen_address_str(self):
        return urllib.parse.urlunparse(self._listen_address)

    async def start(self):
        log.warning("Starting api server at %s", self._listen_address_str)
        runner = web.AppRunner(self._app)
        await runner.setup()
        site = web.TCPSite(runner, host=self._listen_address.hostname, port=self._listen_address.port)
        await site.start()


    @staticmethod
    @web.middleware
    async def _catch_json_decode_error(request, handler):
        try:
            return await handler(request)
        except json.decoder.JSONDecodeError:
            raise web.HTTPBadRequest(body=json.dumps({'message': "Could not decode json body"}))


    @staticmethod
    @web.middleware
    async def _catch_marshmallow_error(request, handler):
        try:
            return await handler(request)
        except marshmallow.exceptions.ValidationError as ex:
            log.exception("Could not deserialize body")
            raise web.HTTPBadRequest(body=json.dumps({'message': 'Bad data format', 'details': ex.messages}))


    async def _translate_clearing_request(self, from_mp: List[ClearingRequestItemFromMarketplace]) -> ClearingRequest:
        offers = []
        bids = []
        for mp_item in from_mp:
            action_type = await self._id_translator.numeric_id_to_action_type(mp_item.actionTypeid)
            cpu = LoadValue('cpu', mp_item.cpu, 'cpu')
            ram = LoadValue('ram', mp_item.ram, 'MB')
            disk = LoadValue('disk', mp_item.disk, 'GB')
            item = ClearingRequestItem(id=mp_item.id, date=mp_item.date, dc_id=mp_item.marketActorid,
                                       starttime=mp_item.actionStartTime, endtime=mp_item.actionEndTime,
                                       price=mp_item.price, action_type=action_type, load_values=[cpu, ram, disk],
                                       target_server=None)

            if action_type == MarketplaceActionType.OFFER:
                offers.append(item)
            else:
                bids.append(item)

        request_id = int((datetime.utcnow() - datetime.fromtimestamp(0)).total_seconds())
        return ClearingRequest(id=request_id, offers=offers, bids=bids, orig_request=from_mp)


    async def _translate_clearing_request_v2(self, from_mp: List[ClearingRequestItemFromMarketplaceV2]) -> ClearingRequest:
        offers = []
        bids = []
        for mp_item in from_mp:
            mp_action = mp_item.marketaction
            mp_priority = mp_item.priority
            action_type = await self._id_translator.numeric_id_to_action_type(mp_action.actionTypeid)
            cpu = LoadValue('cpu', mp_action.cpu, 'cpu')
            ram = LoadValue('ram', mp_action.ram, 'MB')
            disk = LoadValue('disk', mp_action.disk, 'GB')
            item = ClearingRequestItem(id=mp_action.id, date=mp_action.date, dc_id=mp_action.marketActorid,
                                       starttime=mp_action.actionStartTime, endtime=mp_action.actionEndTime,
                                       price=mp_action.price, action_type=action_type, load_values=[cpu, ram, disk],
                                       target_server=None, priority=mp_priority)

            if action_type == MarketplaceActionType.OFFER:
                offers.append(item)
            else:
                bids.append(item)

        request_id = random.randint(1, 2**64)  # I won't even discuss the probability of collision
        return ClearingRequest(id=request_id, offers=offers, bids=bids, orig_request=from_mp)



    async def _post_clearing_request(self, request):
        log.info("Getting a clearing request v1")
        clearing_request_list = await request.json()
        clearing_request_from_mp = [ClearingRequestItemFromMarketplace.Schema(unknown='EXCLUDE').load(item)
                                    for item in clearing_request_list]
        clearing_request = await self._translate_clearing_request(clearing_request_from_mp)
        await self._load_balancer.add_clearing_request(clearing_request)
        return json_response({'id': clearing_request.id}, status=HTTPStatus.CREATED)


    async def _post_clearing_request_v2(self, request):
        log.info("Getting a clearing request v2")
        clearing_request_list = await request.json()
        clearing_request_from_mp = [ClearingRequestItemFromMarketplaceV2.Schema(unknown='EXCLUDE').load(item)
                                    for item in clearing_request_list]
        clearing_request = await self._translate_clearing_request_v2(clearing_request_from_mp)
        response_queue = asyncio.Queue()
        await self._load_balancer.add_and_wait_clearing_request(clearing_request, response_queue)
        (_, _, raw_placement) = await response_queue.get()
        json_matched_bids_offers = MarketplaceClientHttp.create_clearing_results_response(clearing_request, raw_placement)
        return json_response(json_matched_bids_offers, status=HTTPStatus.OK)


    async def _get_all_past_clearings(self, request):  # pylint: disable=unused-argument
        clearings = await self._load_balancer.persistor.get_past_clearings()
        return json_response(clearings, status=HTTPStatus.OK)


    async def _get_clearing_request_view(self, request):
        return await self._get_clearing_view(request, False)

    async def _get_clearing_result_view(self, request):
        return await self._get_clearing_view(request, True)

    async def _get_clearing_view(self, request, show_result):
        clearing_id = int(request.match_info["clearing_id"])
        all_clearing_ids = await self._load_balancer.persistor.get_past_clearing_ids()
        clearing = await self._load_balancer.persistor.get_past_clearing(clearing_id)
        view_generator = ViewGenerator(clearing.request if clearing else None,
                                       clearing.result if clearing and show_result else None,
                                       clearing_id, all_clearing_ids)
        html = view_generator.build_html()
        return web.Response(text=html, content_type='text/html')


class MarketplaceActionType:
    OFFER = 'offer'
    BID = 'bid'



class DASMarketplaceNumericIdToSemanticMapper(ABC):
    @abstractmethod
    async def numeric_id_to_action_type(self, action_id: int) -> str:
        pass

class DASMarketplaceNumericIdToSemanticMapperFake(DASMarketplaceNumericIdToSemanticMapper):
    async def numeric_id_to_action_type(self, action_id: int) -> str:
        if action_id == 1:
            return MarketplaceActionType.OFFER
        elif action_id == 2:
            return MarketplaceActionType.BID


class DASMarketplaceNumericIdToSemanticMapperHttp(DASMarketplaceNumericIdToSemanticMapper, MarketplaceHttp):
    def __init__(self, remote_endpoint, user, password):
        MarketplaceHttp.__init__(self, remote_endpoint, user, password)
        self._cache = dict()
        self._lock = asyncio.Lock()

    async def numeric_id_to_action_type(self, action_id: int) -> str:
        async with self._lock:
            translated = self._cache.get(action_id)
            if translated:
                return translated
        async with self._session() as session:
            translated = None
            query_uri = urljoin(self._remote_endpoint, '/actiontypes/')
            response = await session.get(query_uri)
            response.raise_for_status()
            translation_list = await response.json()
            for translation in translation_list:
                if int(translation.get('id', -1)) == action_id:
                    translated = translation['type']
                    break

            if not translated:
                log.error("Failed to translate ")

            async with self._lock:
                self._cache[action_id] = translated
            return translated
