import aiohttp
import asyncio
import itertools
import logging as log

from abc import ABC, abstractmethod
from typing import List, Tuple

from .clearing import ClearingRequest, ClearingResult
from .clearing_fte import ITLoad, PastClearing, RawPlacement


class IPersistor(ABC):
    @abstractmethod
    async def add_clearing_request(self, clearing_request: ClearingRequest) -> int:
        pass

    @abstractmethod
    async def add_clearing_result(self, clearing_request: ClearingRequest, clearing_result: ClearingResult) -> None:
        pass

    @abstractmethod
    async def get_completed_requests(self) -> List[Tuple[ClearingRequest, ClearingResult]]:
        pass

    @abstractmethod
    async def get_new_requests(self) -> List[ClearingRequest]:
        pass

    @abstractmethod
    async def set_clearing_request_running(self, idx: int):
        pass

    @abstractmethod
    async def set_all_uncompleted_as_new(self):
        pass

    @abstractmethod
    async def get_past_clearings(self):
        pass

    @abstractmethod
    async def archive_request_with_result(self, idx: int, result_for_mock: ClearingResult,
                                          raw_placement: List[RawPlacement]):
        pass


class MemoryPersistor(IPersistor):
    def __init__(self):
        self._new_clearing_requests = {}
        self._running_clearing_requests = {}
        self._clearing_results = {}
        self._completed_clearings = {}
        self._lock = asyncio.Lock()

    @property
    def _all_clearing_requests(self):
        return itertools.chain(self._new_clearing_requests, self._running_clearing_requests)


    async def add_clearing_request(self, clearing_request: ClearingRequest) -> int:
        async with self._lock:
            if (clearing_request.id in self._new_clearing_requests or
               clearing_request.id in self._running_clearing_requests):
                raise aiohttp.web.HTTPBadRequest(
                    reason="Clearing request {} already exists".format(clearing_request.id))
            self._new_clearing_requests[clearing_request.id] = clearing_request
        return clearing_request.id


    async def add_clearing_result(self, clearing_request: ClearingRequest, clearing_result: ClearingResult) -> None:
        async with self._lock:
            self._clearing_results[clearing_request.id] = clearing_result


    async def get_completed_requests(self) -> List[Tuple[ClearingRequest, ClearingResult]]:
        async with self._lock:
            return [(self._clearing_results[res.id], res)
                    for res in self._running_clearing_requests
                    if res.id in self._clearing_results]


    async def get_new_requests(self) -> List[ClearingRequest]:
        async with self._lock:
            return [req for req in self._new_clearing_requests]


    async def get_all_requests(self) -> List[ClearingRequest]:
        async with self._lock:
            return [req for req in self._all_clearing_requests]


    async def set_clearing_request_running(self, idx: int):
        async with self._lock:
            req = self._new_clearing_requests.pop(idx)
            if req:
                self._running_clearing_requests[idx] = req
            else:
                log.warning("Requested to set clearing request %s as running, but not found in the new requests", idx)


    async def set_all_uncompleted_as_new(self):
        async with self._lock:
            self._new_clearing_requests.update(self._running_clearing_requests)
            self._running_clearing_requests.clear()


    async def remove_clearing_request(self, idx: int):
        async with self._lock:
            return self._new_clearing_requests.pop(idx) or self._running_clearing_requests.pop(idx)


    async def archive_request_with_result(self, idx: int, result_for_mock: ClearingResult,
                                          raw_placement: List[RawPlacement]):
        async with self._lock:
            request = self._running_clearing_requests.get(idx)
            if not request:
                log.error("Cannot archive clearing %s: not found", idx)
                return
            self._completed_clearings[idx] = PastClearing(request, result_for_mock, raw_placement)
            self._running_clearing_requests.pop(idx)


    async def get_past_clearings(self):
        async with self._lock:
            return {clearing_id: clearing for clearing_id, clearing in self._completed_clearings.items()}


    async def get_past_clearing_ids(self):
        async with self._lock:
            return list(self._completed_clearings.keys())


    async def get_past_clearing(self, clearing_id: int):
        async with self._lock:
            return self._completed_clearings.get(clearing_id)
