import asyncio
import logging as log

from concurrent.futures import ProcessPoolExecutor
from dataclasses import dataclass
from typing import Dict, List

from .clearing import IClearingStrategy, ClearingRequest, ClearingResult, ClearingStats
from .clearing_fte import Server, ITLoad, RawPlacement
from .marketplace import IMarketplaceClient
from .persist import IPersistor
from ..common.utils import jsonify


class LoadBalancerDaemon:
    class PeriodicIteration:
        pass

    @dataclass
    class ClearingResultMessage:
        request: ClearingRequest
        result: ClearingResult
        raw_placement: Dict[Server, List[ITLoad]]

    def __init__(self, conf, marketplace_client: IMarketplaceClient, persistor: IPersistor,
                 clearing_strategy: IClearingStrategy):
        self._conf = conf
        self._marketplace_client = marketplace_client
        self.persistor = persistor
        self._clearing_strategy = clearing_strategy
        self._clearing_executor = ProcessPoolExecutor(self._max_workers)
        self._synchronous_clearing_requests = dict()
        self._synchronous_clearing_requests_lock = asyncio.Lock()
        self._events_queue: asyncio.Queue = asyncio.Queue()


    @property
    def _max_workers(self):
        return self._conf.get('max_workers', 6)


    @property
    def _main_loop_period(self):
        return self._conf.get('main_loop_period', 5)


    def start(self):
        ticker_task = asyncio.create_task(self._ticker())
        main_loop_task = asyncio.create_task(self._main_loop())
        return asyncio.gather(ticker_task, main_loop_task)


    async def add_clearing_request(self, clearing_request: ClearingRequest) -> int:
        ''' Pass bids and offers to be matched together, save them in some persistence layer,
            returns an id for the clearing. '''
        req_id = await self.persistor.add_clearing_request(clearing_request)
        await self._events_queue.put(clearing_request)
        return req_id


    async def add_and_wait_clearing_request(self, clearing_request: ClearingRequest,
                                            response_queue: asyncio.Queue) -> ClearingResult:
        clearing_id = await self.add_clearing_request(clearing_request)
        async with self._synchronous_clearing_requests_lock:
            self._synchronous_clearing_requests[clearing_id] = response_queue
            log.debug("Existing synchronous request queues is now: %s", self._synchronous_clearing_requests)


    async def _report_clearing_result(self, event: ClearingResultMessage) -> None:
        async with self._synchronous_clearing_requests_lock:
            result_queue = self._synchronous_clearing_requests.get(event.request.id)

        if result_queue:
            log.info("Report clearing result to a queue (clearing id: %d)", event.request.id)
            await result_queue.put((event.request, event.result, event.raw_placement))
        else:
            log.info("No queue found for synchronous request %d, call into marketplace", event.request.id)
            log.debug("Existing synchronous request queues: %s", self._synchronous_clearing_requests)
            await self._marketplace_client.report_clearing_result(event.request, event.result, event.raw_placement)


    async def _ticker(self):
        while True:
            await asyncio.sleep(self._main_loop_period)
            await self._events_queue.put(self.PeriodicIteration())


    def _start_clearing(self, clearing_request: ClearingRequest) -> None:
        async def launch_in_process():
            try:
                loop = asyncio.get_event_loop()
                log.info("Launching side process for clearing for %s", clearing_request.id)
                for_mock, raw_placement = await loop.run_in_executor(self._clearing_executor,
                                                             self._clearing_strategy.clear_marketplace_round,
                                                             clearing_request)
                log.info("Clearing for %s completed, posting result", clearing_request.id)
                await self._events_queue.put(self.ClearingResultMessage(clearing_request, for_mock, raw_placement))
            except Exception:
                log.exception("Clearing failed for %d: ", clearing_request.id)
                result = ClearingResult(id=clearing_request.id, accepted=[], rejected=clearing_request.offers)
                await self._events_queue.put(self.ClearingResultMessage(clearing_request, result, dict()))

        asyncio.create_task(launch_in_process())
        log.info("Launched clearing task for %d", clearing_request.id)


    async def _main_loop(self):
        log.warning("start main loop")
        # On clearing completion, clearing submission, or every N second:
        #       - look for all not running clearing requests
        #       - report results of completed clearings
        while True:
            try:
                log.debug("Main loop: wait for event")
                event = await self._events_queue.get()
                if isinstance(event, self.PeriodicIteration):
                    log.info("Have a look at all requests")
                elif isinstance(event, ClearingRequest):
                    log.warning("Got new clearing request: %s", event.id)
                    log.info("Clearing request %s: %s", event.id, event.short_str())
                    log.debug("Detailed clearing request: %s", event)
                    await self.persistor.set_clearing_request_running(event.id)
                    self._start_clearing(event)
                elif isinstance(event, self.ClearingResultMessage):
                    stats = ClearingStats(event.request, event.result)
                    log.warning("Got a clearing result for %s: %s", event.request.id, stats)
                    log.info("Clearing result for %s: %s", event.request.id, event.result.short_str())
                    log.debug("Detailed clearing result for %s: %s", event.request, jsonify(event.result, indent=2))
                    await self._report_clearing_result(event)
                    raw_placement = [RawPlacement(server, itloads) for server, itloads in event.raw_placement.items()]
                    await self.persistor.archive_request_with_result(
                        event.request.id, event.result, raw_placement)
                else:
                    log.error("Got unhandled event type: %s - %s", type(event), event)

                log.debug("Done handling event, await the next one")
            except asyncio.CancelledError:
                log.warning("Main loop cancelled, exiting...")
                raise
            except Exception:
                log.exception("Exception in main loop: ")
