import functools
import itertools
import logging as log
import math
import marshmallow_dataclass
import operator
import random

from dataclasses import dataclass, field
from marshmallow import Schema as MSchema
from typing import ClassVar, Dict, List, Type, Set, Tuple

from .clearing import (AcceptedOffers, ClearingRequest, ClearingRequestItem, ClearingResult,
                       IClearingStrategy, IOfferValueComputer)
# pylint: disable=import-error
from ..common import utils


@dataclass
class ITLoad:
    cpu: int
    ram: int
    disk: int
    value: int
    original_item: ClearingRequestItem  # so we can convert back when generating the solution

    def __str__(self):
        return utils.jsonify(self, indent=2, keys_filter=lambda key: key != "original_item")


@dataclass
class Server:
    id: str
    cpu: int
    ram: int
    disk: int
    value: int
    dc_id: str = field(init=False)
    original_item: ClearingRequestItem  # for back mapping

    def __post_init__(self):
        self.dc_id = self.original_item.dc_id

    def __str__(self):
        return utils.jsonify(self, indent=2, keys_filter=lambda key: key != "original_item")

    # We want to make it hashable so it can be a dictionary key type, but
    # we also want to make sure that each server is seen as "unique", which
    # may not be the case if we let dataclass create a default hash based on
    # fields content.
    def __hash__(self):
        return id(self)


@marshmallow_dataclass.dataclass
class RawPlacement(utils.WithSchema):
    server: Server
    itloads: List[ITLoad]
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@marshmallow_dataclass.dataclass
class PastClearing(utils.WithSchema):
    request: ClearingRequest
    result: ClearingResult
    raw_placement: RawPlacement
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy




class ITLoadsSet:
    def __init__(self, dc_id: str, itloads: List[ITLoad] = None):
        self.dc_id = dc_id
        self.itloads = itloads or []

    def __repr__(self):
        return utils.jsonify(self, indent=2, keys_filter=lambda key: key != "original_item")

    @property
    def value(self):
        return sum(itl.value for itl in self.itloads)


class ClearingStrategyFollowTheEnergy(IClearingStrategy):
    def __init__(self, itload_value_computer: IOfferValueComputer, shuffle: bool):
        super().__init__(itload_value_computer)
        self.shuffle = shuffle

    def clear_marketplace_round(self, clearing_request: ClearingRequest) -> ClearingResult:
        itloads, servers = self._convert_request_data(clearing_request)
        log.debug("For clearing %s, got:\n\n***loads***:\n\n %s\n\n*** servers ***:\n%s",
                  clearing_request.id, itloads, servers)
        return self._perform_clearing(clearing_request, itloads, servers)


    def _convert_request_data(self, clearing_request: ClearingRequest) -> Tuple[List[ITLoadsSet], List[Server]]:
        itloads: Dict[str, ITLoadsSet] = {}
        for offer in clearing_request.offers:
            itls_for_dc = itloads.get(offer.dc_id)
            if not itls_for_dc:
                itls_for_dc = ITLoadsSet(offer.dc_id)
                itloads[offer.dc_id] = itls_for_dc
            itl = self._offer_to_itl(offer)
            itls_for_dc.itloads.append(itl)

        servers = []
        for i, bid in enumerate(clearing_request.bids):
            server = self._bid_to_server(i, bid)
            servers.append(server)

        return list(itloads.values()), servers


    def _offer_to_itl(self, offer: ClearingRequestItem):
        cpu, ram, disk = 0, 0, 0
        for load_value in offer.load_values:
            if load_value.parameter == 'cpu':
                cpu = load_value.value
            elif load_value.parameter == 'ram':
                ram = utils.to_mega_bytes(load_value.value, load_value.uom)
            elif load_value.parameter == 'disk':
                disk = utils.to_mega_bytes(load_value.value, load_value.uom)

        return ITLoad(cpu, ram, disk, self._value_computer.compute_value(offer), offer)


    def _bid_to_server(self, i: int, bid: ClearingRequestItem):
        cpu, ram, disk = 0, 0, 0
        for load_value in bid.load_values:
            if load_value.parameter == 'cpu':
                cpu = load_value.value
            elif load_value.parameter == 'ram':
                ram = utils.to_mega_bytes(load_value.value, load_value.uom)
            elif load_value.parameter == 'disk':
                disk = utils.to_mega_bytes(load_value.value, load_value.uom)

        return Server('{}_{}'.format(bid.dc_id, i), cpu, ram, disk,
                      self._value_computer.compute_value(bid), bid)


    def _iterate_over_dc_subsets(self, all_dcs: List[ITLoadsSet]):
        ''' Iterate over the powerset of datacenters offering IT loads in
            hopefully decreasing value (should be mostly OK) '''
        all_dcs.sort(key=lambda dc: dc.value, reverse=True)
        for i in range(len(all_dcs), 0, -1):
            for subset in itertools.combinations(all_dcs, i):
                yield subset


    def _perform_clearing(self, clearing_request: ClearingRequest,
                          itloads: List[ITLoadsSet],
                          servers: List[Server]) -> ClearingResult:
        # We can flatten the servers, the dc id is in original_item anyway,
        # so we can get back to it.
        for considered_itloads in self._iterate_over_dc_subsets(itloads):
            flattened_itloads = list(itertools.chain.from_iterable((itl_set.itloads for itl_set in considered_itloads)))
            if self.shuffle:
                random.shuffle(flattened_itloads)
            name = 'binpacker#{}'.format(','.join(str(itl.dc_id) for itl in considered_itloads))
            vector_bin_packer = VectorBinPacker(name, flattened_itloads, servers)
            raw_placement = vector_bin_packer.find_placement()
            if raw_placement:
                log.debug("%s: found placement with subset %s with value %d: %s",
                          clearing_request.id, [itl_set.dc_id for itl_set in considered_itloads],
                          sum(itl_set.value for itl_set in considered_itloads),
                          utils.jsonify({server.id: itloads for server, itloads in raw_placement.items()},
                                        indent=4, keys_filter=lambda key: key != "original_item"))
                log.warning("Found valid placement for %s with value %d",
                            clearing_request.id, sum(itl_set.value for itl_set in considered_itloads))
                excluded_itloads = list(set(itloads) - set(considered_itloads))
                for_mock = self._clearing_result_for_mock_from_placement(clearing_request, raw_placement, excluded_itloads)
                return for_mock, raw_placement
            else:
                log.debug("%s: no placement with subset %s", clearing_request.id,
                          [itl_set.dc_id for itl_set in considered_itloads])

        return ClearingResult(clearing_request.id, [],
                              [itl.original_item for itl_set in itloads for itl in itl_set.itloads]), dict()



    # Keep that so that we can reuse the old demo UI that used this format.
    # A bit clumsy...
    def _clearing_result_for_mock_from_placement(self, clearing_request: ClearingRequest,
                                                 raw_placement: Dict[Server, List[ITLoad]],
                                                 excluded_itloads: List[ITLoadsSet]) -> ClearingResult:
        target_dcs = set(server.original_item.dc_id for server in raw_placement.keys())
        accepted_per_dc: Dict[str, List[ClearingRequestItem]] = {dc_id: [] for dc_id in target_dcs}
        for server, itloads in raw_placement.items():
            for itl in itloads:
                itl.original_item.target_server = server.id
            accepted_per_dc[server.dc_id].extend(itl.original_item for itl in itloads)

        accepted_offers = [
            # AcceptedOffers(server.dc_id, [itl.original_item for itl in itloads])
            AcceptedOffers(dc_id, itloads)
            for dc_id, itloads in accepted_per_dc.items()
        ]

        rejected_offers = [itl.original_item for itl_set in excluded_itloads for itl in itl_set.itloads]

        return ClearingResult(clearing_request.id, accepted_offers, rejected_offers)




class VectorBinPacker:
    def __init__(self, name: str, itloads: List[ITLoad], servers: List[Server]):
        self.name = name
        self.itloads = itloads
        self.servers = servers
        factor = 0.01
        while True:
            try:
                self.w_cpu = math.exp(factor * sum(itl.cpu for itl in self.itloads) / len(self.itloads))
                self.w_ram = math.exp(factor * sum(itl.ram for itl in self.itloads) / len(self.itloads))
                self.w_disk = math.exp(factor * sum(itl.disk for itl in self.itloads) / len(self.itloads))
                break
            except OverflowError:
                factor /= 10

        self._current_placement: Dict[Server, List[ITLoad]] = {
            server: [] for server in servers
        }
        self._remaining_itloads = list(self.itloads)
        self._remaining_servers = list(self.servers)


    def find_placement(self) -> Dict[Server, List[ITLoad]]:
        for server in sorted(self.servers, key=lambda srv: srv.value, reverse=True):
            log.debug("%s: start filling server %s", self.name, server.id)
            if self._is_trivially_impossible():
                return {}
            if not any(self._remaining_itloads):
                log.info("%s: Placed all IT loads, return found placement", self.name)
                return self._current_placement

            while True:
                itl_to_place = self._find_biggest_fitting_itload(server)
                if not itl_to_place:
                    log.debug("%s: Server %s is full, go to next one", self.name, server.id)
                    break
                else:
                    log.debug("%s: Placing %s on %s", self.name, itl_to_place, server.id)
                    self._place_itload(itl_to_place, server)

        if not any(self._remaining_itloads):
            log.info("%s: Placed all IT loads, return found placement", self.name)
            return self._current_placement
        else:
            log.debug("%s: At the end of placement, there are remaining IT loads: %s", self.name,
                      utils.jsonify(self._remaining_itloads, indent=2,
                                    keys_filter=lambda key: key != "original_item"))
            log.info("%s: Exhausted all servers but some IT loads remain, failing", self.name)
            return {}


    def _remaining_avail_hw(self):
        # Optim: keep that value updated when placing stuff, that would probably be MUCH faster
        return functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                map(self._remaining_on, self._remaining_servers),
                                (0, 0, 0))



    def _remaining_needed_hw(self):
        # Optim: same here. Doing that at all iteration is probably quite heavy.
        return functools.reduce(lambda a, b: tuple(map(operator.add, a, b)),
                                map(self._get_itload_hw, self._remaining_itloads),
                                (0, 0, 0))


    def _is_trivially_impossible(self):
        def diag_impossible(avail, needed, what):
            if avail < needed:
                log.info("%s: No longer enough %s to place all it loads (avail: %d, needed: %d)",
                         self.name, what, avail, needed)
                return True
            return False

        avail_cpu, avail_ram, avail_disk = self._remaining_avail_hw()
        needed_cpu, needed_ram, needed_disk = self._remaining_needed_hw()

        return (diag_impossible(avail_cpu, needed_cpu, 'CPU') or
                diag_impossible(avail_ram, needed_ram, 'RAM') or
                diag_impossible(avail_disk, needed_disk, 'disk'))



    def _get_itload_hw(self, itload: ITLoad):
        return itload.cpu, itload.ram, itload.disk


    def _remaining_on(self, server: Server) -> Tuple[int, int, int]:
        rem_cpu = server.cpu - sum(itl.cpu for itl in self._current_placement.get(server, []))
        rem_ram = server.ram - sum(itl.ram for itl in self._current_placement.get(server, []))
        rem_disk = server.disk - sum(itl.disk for itl in self._current_placement.get(server, []))

        assert rem_cpu >= 0
        assert rem_ram >= 0
        assert rem_disk >= 0

        return rem_cpu, rem_ram, rem_disk


    def _place_itload(self, itload: ITLoad, server: Server):
        self._current_placement[server].append(itload)
        self._remaining_itloads.remove(itload)


    def _find_biggest_fitting_itload(self, server: Server):
        def fits(itload):
            def diag_not_enough(avail, needed, what):
                if avail < needed:
                    log.debug("%s: Not enough remaining %s on %s for %s (%d < %d)", self.name,
                              what, server.id, itload, avail, needed)
                    return False
                return True

            rem_cpu, rem_ram, rem_disk = self._remaining_on(server)

            return (diag_not_enough(rem_cpu, itload.cpu, 'CPU') and
                    diag_not_enough(rem_ram, itload.ram, 'RAM') and
                    diag_not_enough(rem_disk, itload.disk, 'disk'))

        max_itl = None
        max_remdot = 0
        seen: Set[Tuple[int, int, int]] = set()
        for itl in self._remaining_itloads:
            if (itl.cpu, itl.ram, itl.disk) in seen:
                # IT loads with the same size are indistinguishable, so only consider
                # one of each.
                continue

            if fits(itl):
                remdot = self._remdot(itl, server)
                if remdot > max_remdot:
                    max_remdot = remdot
                    max_itl = itl
                seen.add((itl.cpu, itl.ram, itl.disk))
        return max_itl


    def _remdot(self, itload: ITLoad, server: Server):
        return (self.w_cpu * server.cpu * itload.cpu +
                self.w_ram * server.ram * itload.ram +
                self.w_disk * server.disk * itload.disk)
