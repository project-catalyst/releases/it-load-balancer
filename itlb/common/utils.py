import collections
import json

from aiohttp import web
from datetime import datetime
from functools import reduce, singledispatch
from urllib.parse import urljoin


@singledispatch
def to_json_serializable(obj, keys_converter, keys_filter):
    if hasattr(obj, '__dict__'):
        return to_json_serializable(obj.__dict__, keys_converter, keys_filter)
    # This is the final default: we did not end in a special
    # case, and there is no __dict__, so we are probably with
    # a base type like str or float, that json.dumps will happily
    # serialize
    return obj


@to_json_serializable.register(type(None))
def _1(n, keys_converter, keys_filter):
    return None


@to_json_serializable.register(dict)
def _2(d, keys_converter, keys_filter):
    keys_filter_ = keys_filter or (lambda x: True)
    if keys_converter is None:
        return {k: to_json_serializable(v, keys_converter, keys_filter) for k, v in d.items() if keys_filter_(k)}
    else:
        return {keys_converter(k): to_json_serializable(v, keys_converter, keys_filter)
                for k, v in d.items() if keys_filter_(k)}


@to_json_serializable.register(str)
def _3(s, keys_converter, keys_filter):
    return s


@to_json_serializable.register(collections.Iterable)
def _4(lst, keys_converter, keys_filter):
    return [to_json_serializable(item, keys_converter, keys_filter) for item in lst]


@to_json_serializable.register(datetime)
def _5(date, keys_converter, keys_filter):
    return date.isoformat()


def jsonify(obj, keys_converter=None, keys_filter=None, **kwargs):
    if obj is None:
        return json.dumps(None, **kwargs)

    serializable = to_json_serializable(obj, keys_converter, keys_filter)
    return json.dumps(serializable, sort_keys=True, **kwargs)


class WithSchema:
    def __str__(self):
        return self.json()

    def json(self):
        # pylint: disable=no-member
        return self.Schema().dumps(self)

    def json_obj(self):
        # pylint: disable=no-member
        return self.Schema().dump(self)


def json_response(obj, **kwargs):
    if isinstance(obj, WithSchema):
        return web.json_response(obj.json_obj(), **kwargs)
    else:
        def dumps(o):
            return jsonify(o, keys_filter=lambda key: not (isinstance(key, str) and key.startswith('_')))
        return web.json_response(obj, dumps=dumps, **kwargs)


def to_mega_bytes(size, unit):
    if unit is None or unit == '' or unit.lower() == 'b' or unit.lower() == 'bytes':
        return size / (1024 * 1024)
    elif unit.lower() == 'kb':
        return size / 1024
    elif unit.lower() == 'mb':
        return size
    elif unit.lower() == 'gb':
        return size * 1024
    elif unit.lower() == 'tb':
        return size * 1024 * 1024
    else:
        raise KeyError("Unknown unit '{}'".format(unit))


def urljoin_many(*args):
    return reduce(urljoin, [(a + '/') for a in args[:-1]] + [args[-1]])
