import aiohttp
import asyncio
import copy
import json
import logging as log
import pathlib

from abc import ABC, abstractmethod
from asyncio import Queue
from datetime import datetime, timedelta, timezone
from enum import Enum
from marshmallow import Schema as MSchema
from marshmallow_dataclass import dataclass
from typing import ClassVar, List, Optional, Type

from ..common.utils import urljoin_many, WithSchema


# ########## From Tudor's e-mail
#
# Type of actions requested in an Energy Optimiser Plan:
#   - Time shifting of x Kwh of delay tolerant IT load (in the same DC) from time interval
#     T1 to a later time interval T2.
#           => EOPlanShiftIT
#   - Relocate x Kwh of IT load to other DC in the time interval T1.
#           => EOPlanRelocateIT
#   - Get x of IT load from another DC in the time interval T1.
#           => EOPlanAcceptIT
#
# [...] there are other actions but that are of no interest to the IT load balancer


# ######### Notes on workflow to get EO plans.
#
# According to D2.3, the Intra DC Energy Optimizer provides an  endpoint
#                   GET /optimisation/notify-plan
# with as description:
#   Through this interface, other components are notified that the
#   optimization plan computation was finished, and they can get the
#   action plan from the Data Storage
#
# This is just a GET route, to "notification" is quite an overstatement.
# The return data format is undocumented (as far as I can find), so let's guess
# something for the moment, like it returning the plan start and end time (needed
# to get the plan, see next).
#
# Then the DB API specifies GET /action/shiftWorkload/{dataCenterID}/{startTime}/{endTime}:
#
#   Through this interface, other components may retrieve information
#   related to different actions that can be taken for energy optimization
#   inside a DC
#
# It does not specify anything about 'relocateWorkload' and 'getWorkload' but we'll
# assume they exist. I don't know exactly either where we are supposed to get the
# DC ID (let's say config) and the start / end time (let's stay by pollig the EO,
# I really don't see otherwise).
#
#
# The imagined workflow is hence grossly as follows:
#
#   dc_id = self._config.dc_id
#   forever every N seconds:
#       plan_start, plan_end = http.get(http://energy-optimiser/optimisation/notify-plan)
#       if plan_start and plan_end:
#           shift = http.get(http://db-api/action/shiftWorkload/{dc_id}/{plan_start}/{plan_end}
#           relocate = http.get(http://db-api/action/shiftWorkload/{dc_id}/{plan_start}/{plan_end}
#           accept = http.get(http://db-api/action/shiftWorkload/{dc_id}/{plan_start}/{plan_end}
#
#       main_daemon.notify_eo_plan(shift, relocate, accept)





@dataclass
class TimeInterval(WithSchema):
    start: datetime
    end: datetime
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@dataclass
class EOPlanShiftIT(WithSchema):
    id: str
    start: datetime  # start of interval where action is valid (i.e. from what interval workload is shifted)
    end: datetime    # end of interval where action is valid (i.e. from what interval workload is shifted)
    name: str        # "Relocate Workload (KWh)", osef
    value: float       # equivalent energy value of workload to be shifted (in kW.h)
    active: bool     # if set to false, won't be executed. Don't get it...
    destinationDC: str  # osef, will always be the current DC
    sdtwStartTime: datetime  # start of interval where workload is moved to be executed (i.e. to what interval workload is shifted)
    sdtwEndTime: datetime  # end of interval where workload is moved to be executed (i.e. to what interval workload is shifted)
    sdtwPercentage: float  # percentage of workload to be moved from interval [start,end] - is equivalent to value/(totalWorkload during interval [start,end] *100
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    @property
    def power_watts(self):
        # value is in kW.h, so we first get energy in watt.second, then divide it by
        # the period expressed in seconds ot have an average power in watts
        return (self.value * 1000 * 3600) / (self.end - self.start).total_seconds()


@dataclass
class EOPlanRelocateIT(WithSchema):
    id: str
    start: datetime  # start of interval where action is valid (i.e. from what interval workload is shifted)
    end: datetime    # end of interval where action is valid (i.e. from what interval workload is shifted)
    name: str        # "Relocate Workload (KWh)", osef
    value: float       # equivalent energy value of workload to be shifted (in KWh)
    active: bool     # if set to false, won't be executed. Don't get it...
    sdtwPercentage: float  # percentage of workload to be moved from interval [start,end] - is equivalent to value/(totalWorkload during interval [start,end] *100
    move_from_date: float
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    @property
    def power_watts(self):
        # value is in kW.h, so we first get energy in watt.second, then divide it by
        # the period expressed in seconds ot have an average power in watts
        return (self.value * 1000 * 3600) / (self.end - self.start).total_seconds()


@dataclass
class EOPlanAcceptIT(WithSchema):
    id: str
    start: datetime  # start of interval where action is valid (i.e. from what interval workload is shifted)
    end: datetime    # end of interval where action is valid (i.e. from what interval workload is shifted)
    name: str        # osef
    value: float       # equivalent energy value of workload to be shifted (in KWh)
    active: bool     # if set to false, won't be executed. Don't get it...
    sdtwPercentage: float  # percentage of workload to be moved from interval [start,end] - is equivalent to value/(totalWorkload during interval [start,end] *100
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    @property
    def power_watts(self):
        # value is in kW.h, so we first get energy in watt.second, then divide it by
        # the period expressed in seconds ot have an average power in watts
        return (self.value * 1000 * 3600) / (self.end - self.start).total_seconds()


class EOPlanStatus(Enum):
    PENDING = "pending"  # submitted but not yet treated
    EXAMINATION = "examination"  # Being treated, sending offers, sending bids, waiting for
                                 # marketplace round completion, ...  # noqa
    IMPLEMENTING = "implementing"  # Has been accepted, now being implemented (actually shifting
                                   # or migrating IT load, waiting for confirmation that it worked  # noqa
    APPLIED = "applied"  # accepted and implemented, we can report success to the EO
    REJECTED = "rejected"  # could not be applied, we can report failure to the EO


@dataclass
class EOPlan(WithSchema):
    shift: Optional[EOPlanShiftIT] = None
    relocate: Optional[EOPlanRelocateIT] = None
    accept: Optional[EOPlanAcceptIT] = None
    status: EOPlanStatus = EOPlanStatus.PENDING
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


    @property
    def aggregate_id(self):
        shift_id = self.shift.id if self.shift else "none"
        relocate_id = self.relocate.id if self.relocate else "none"
        accept_id = self.accept.id if self.accept else "none"

        return f"{shift_id}_{relocate_id}_{accept_id}"



# @dataclass
# class EOPlanNotificationResult(WithSchema):
#     dc_id: str
#     interval: TimeInterval
#     Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy



class EOPlanSource(ABC):
    # @abstractmethod
    # async def get_eo_plan_notification(self) -> EOPlanNotificationResult:
    #     pass

    @abstractmethod
    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        pass





class EOPlanPoller():
    def __init__(self, period_sec: int, dc_id: str, eo_plan_source: EOPlanSource, message_queue: Queue,
                 forced_timestamp_ms: int=0):
        self.eo_plan_source = eo_plan_source
        self._period_sec = period_sec
        self._dc_id = dc_id
        self._message_queue = message_queue
        self._forced_timestamp_ms = forced_timestamp_ms

    async def loop(self):
        all_seen_plans = set()
        while True:
            log.debug("EOPlanPoller: iteration")
            if self._forced_timestamp_ms == 0:
                # next_hour_start, next_hour_end = self._next_hour_utc()
                next_hour_start, next_hour_end = self._same_hour_tomorrow_utc()
                eo_plans = await self.eo_plan_source.get_eo_plans(self._dc_id, next_hour_start, next_hour_end)
            else:
                start = datetime.fromtimestamp(self._forced_timestamp_ms / 1000).replace(tzinfo=timezone.utc)
                end = start + timedelta(hours=1)
                eo_plans = await self.eo_plan_source.get_eo_plans(self._dc_id, start, end)

            for eo_plan in eo_plans:
                if eo_plan.aggregate_id not in all_seen_plans:
                    log.warning("Got a new EO plan: %s", eo_plan)
                    all_seen_plans.add(eo_plan.aggregate_id)
                    await self._message_queue.put(eo_plan)
                else:
                    log.info("Got EO plan %s, which was already seen, skip", eo_plan.aggregate_id)

            await asyncio.sleep(self._period_sec)



    def _next_hour_utc(self):
        next_hour_start = (datetime.now(timezone.utc) + timedelta(hours=1)).replace(minute=0, second=0, microsecond=0)
        next_hour_end = next_hour_start + timedelta(hours=1)

        return next_hour_start, next_hour_end

        # next_hour_start_unix_ms = (next_hour_start - datetime.fromtimestamp(0)).total_seconds() * 1000
        # next_hour_end_unix_ms = (next_hour_end - datetime.fromtimestamp(0)).total_seconds() * 1000

        # return next_hour_start_unix_ms, next_hour_start_unix_ms

    def _same_hour_tomorrow_utc(self):
        same_hour_tomorrow_start = (datetime.now(timezone.utc) + timedelta(days=1, hours=1)).replace(minute=0, second=0, microsecond=0)
        same_hour_tomorrow_end = same_hour_tomorrow_start + timedelta(hours=1)

        return same_hour_tomorrow_start, same_hour_tomorrow_end



# These are fake stuff for mocking

class EOPlanSourceOneFake(EOPlanSource):
    def __init__(self):
        self.dc_id = "this-dc"

        self._already_returned = False

    # async def get_eo_plan_notification(self):
    #     if not self._already_returned:
    #         self._already_returned = True
    #         return EOPlanNotificationResult(self.dc_id, self.from_interval)
    #     else:
    #         return None

    @abstractmethod
    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        pass


class EOPlanSourceOneFakeRelocate(EOPlanSourceOneFake):
    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:

        return [EOPlan(
                shift=None,
                relocate=EOPlanRelocateIT(id="relocate-id", start=start, end=end, name="relocate stuff",
                                         value=0.06, sdtwPercentage=1, active=True),
                                         # value=10, sdtwPercentage=1, active=True),
                accept=None)]



class EOPlanSourceOneFakeGet(EOPlanSourceOneFake):
    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        return [EOPlan(
               shift=None,
               relocate=None,
               accept=EOPlanAcceptIT(id="accept-id", start=start, end=end, name="accept stuff",
                                     value=0.20, sdtwPercentage=1, active=True))]


class EOPlanSourceHttpTrigger(EOPlanSource):
    def __init__(self):
        self.app = aiohttp.web.Application()
        self.app.add_routes([
            aiohttp.web.post("/new", self._add_eo_plan),
        ])

        self._last_eo_plan: EOPlan = EOPlan()


    async def _add_eo_plan(self, request):
        json_plan = await request.json()
        plan = EOPlan.Schema().load(json_plan)

        self._last_eo_plan = plan
        return aiohttp.web.Response(status=200)



    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        # Manipulate the dates in the submitted EO plan so that it'll fit
        plan = copy.deepcopy(self._last_eo_plan)
        if plan.relocate:
            plan.relocate.start = start
            plan.relocate.end = end
        if plan.accept:
            plan.accept.start = start
            plan.accept.end = end
        if plan.shift:
            plan.shift.start = start
            plan.shift.end = end

        return [plan]



# class EOPlanSourceHttpTriggerReload(EOPlanSource):
#     def __init__(self, dc_id, eo_plan_source, message_queue):
#         self._message_queue = message_queue
#         self.eo_plan_source = eo_plan_source
#         self._dc_id = dc_id
#         self.app = aiohttp.web.Application()
#         self.app.add_routes([
#             aiohttp.web.post("/trigger", self._add_eo_plan),
#         ])
#
#         self._triggered = False
#
#
#     async def _add_eo_plan(self, request):
#         self._triggered = True
#         return aiohttp.web.Response(status=200)
#
#
#     async def loop(self):
#         while True:
#             await asyncio.sleep(1)
#             if self._triggered:
#                 next_hour_start, next_hour_end = self._same_hour_tomorrow_utc()
#                 eo_plan = await self.eo_plan_source.get_eo_plan(self._dc_id, next_hour_start, next_hour_end)
#                 self._message_queue.post(eo_plan)
#             else:
#                 asyncio.sleep(1)





@dataclass
class EOPlanItemFromEO(WithSchema):
    id: str
    type: str
    startTime: int  # start of interval where action is valid (i.e. from what interval workload is shifted)
    endTime: int    # end of interval where action is valid (i.e. from what interval workload is shifted)
    amount: float     # equivalent energy value of workload to be shifted (in KWh)
    moveFromDate: int
    movePercentage: float
    active: bool
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy



class EOPlanSourceHttp(EOPlanSource):
    def __init__(self, *, query_plan_uri):
        self.query_plan_uri = query_plan_uri
        self._http_session = aiohttp.ClientSession()


    def timestamp_ms_to_datetime(self, ms_from_epoch):
        return datetime.fromtimestamp(int(ms_from_epoch / 1000)).replace(tzinfo=timezone.utc)


    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        # raise NotImplementedError("web source for EO plan not yet implemented")
        try:
            timestamp_sec = (start - datetime(1970, 1, 1).replace(tzinfo=timezone.utc)).total_seconds()
            timestamp_ms = int(timestamp_sec * 1000)
            url = urljoin_many(self.query_plan_uri, str(timestamp_ms), "0.0")
            log.info("Getting eo plans from %s (%s) from url %s", start, timestamp_ms, url)
            resp = await self._http_session.get(url)
            raw_plan = await resp.json()
            log.debug("Raw plan from %s (%f): %s", start, timestamp_ms, raw_plan)
            plan_from_eo = [EOPlanItemFromEO.Schema(unknown='EXCLUDE').load(raw_plan_item) for raw_plan_item in raw_plan]
            shifting_plans_for_relocation = [
                EOPlanRelocateIT(
                    id=plan.id, start=self.timestamp_ms_to_datetime(plan.startTime),
                    end=self.timestamp_ms_to_datetime(plan.endTime), name=plan.type, value=plan.amount,
                    active=plan.active, sdtwPercentage=plan.movePercentage,
                    move_from_date=plan.moveFromDate)
                for plan in plan_from_eo if plan.type == 'Relocate Load (KWh)'
            ]

            whole_plans = [EOPlan(None, shifting_plan, None) for shifting_plan in shifting_plans_for_relocation]
            # return [whole_plans[0]]
            return whole_plans
        except Exception:
            log.exception("Exception while querying eo plan notification")
            return []



class EOPlanSourceFile(EOPlanSource):
    def __init__(self, *, plan_file_path):
        self.plan_file_path = plan_file_path

    def timestamp_ms_to_datetime(self, ms_from_epoch):
        return datetime.fromtimestamp(int(ms_from_epoch / 1000)).replace(tzinfo=timezone.utc)

    async def get_eo_plans(self, dc_id: str, start: datetime, end: datetime) -> List[EOPlan]:
        # raise NotImplementedError("web source for EO plan not yet implemented")
        try:
            path = pathlib.PosixPath(self.plan_file_path)
            raw_plan_txt = path.read_text()
            raw_plan = json.loads(raw_plan_txt)
            log.debug("Raw plan from %s: %s", self.plan_file_path, raw_plan)
            plan_from_eo = [EOPlanItemFromEO.Schema(unknown='EXCLUDE').load(raw_plan_item) for raw_plan_item in raw_plan]

            shifting_plans_for_relocation = [
                EOPlanRelocateIT(
                    id=plan.id, start=self.timestamp_ms_to_datetime(plan.startTime),
                    end=self.timestamp_ms_to_datetime(plan.endTime), name=plan.type, value=plan.amount,
                    active=plan.active, sdtwPercentage=plan.movePercentage,
                    move_from_date=plan.moveFromDate)
                for plan in plan_from_eo if plan.type == 'Relocate Load (KWh)'
            ]

            whole_plans = [EOPlan(None, shifting_plan, None) for shifting_plan in shifting_plans_for_relocation]
            # return [whole_plans[0]]
            return whole_plans
        except Exception:
            log.exception("Exception while querying eo plan notification")
            return []
