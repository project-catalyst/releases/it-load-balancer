from abc import ABC, abstractmethod
from typing import Tuple


class ITEMapper(ABC):
    @abstractmethod
    def get_watts_for_it(self, cpu: int, ram: int, disk: int) -> float:
        pass


class ITEMapperFixedByCpu(ITEMapper):
    def __init__(self, watts_per_cpu):
        self.watts_per_cpu = watts_per_cpu

    def get_watts_for_it(self, cpu: int, ram: int, disk: int) -> float:
        return cpu * self.watts_per_cpu

    def get_it_for_watts(self, watts: float) -> Tuple[int, int, int]:
        # We use "-1" for ram and cpu to signal "no restriction"
        return int(watts / self.watts_per_cpu), -1, -1
