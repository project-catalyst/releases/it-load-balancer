from abc import ABC, abstractmethod
from datetime import datetime


class PricingPolicy(ABC):
    @abstractmethod
    def price_for_it(self, start: datetime, end: datetime, cpu: int, ram_mb: int, disk_gb: int) -> float:
        pass

class PricingPolicyFixed(PricingPolicy):
    def __init__(self, *, per_cpu_hour: float=0, per_ram_mb_hour: float=0, per_disk_gb_hour: float=0):
        self.per_cpu_hour = per_cpu_hour
        self.per_ram_mb_hour = per_ram_mb_hour
        self.per_disk_gb_hour = per_disk_gb_hour


    def price_for_it(self, start: datetime, end: datetime, cpu: int, ram_mb: int, disk_gb: int) -> float:
        duration_hours = (end - start).total_seconds() / 3600.0
        price_per_hour = cpu * self.per_cpu_hour + ram_mb * self.per_ram_mb_hour + disk_gb * self.per_disk_gb_hour
        return price_per_hour * duration_hours
