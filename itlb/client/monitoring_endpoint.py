import json
import logging as log
import urllib.parse

from aiohttp import web
from http import HTTPStatus

from .monitoring_ui import ViewGenerator
from .persist import EOPlanImplementation, EOPlanRegistry
from ..common.utils import json_response


class MonitoringEndpointApp:

    class Hello:
        pass

    def __init__(self, *, eo_plan_registry: EOPlanRegistry, listen_address, eo_plan_pusher: web.Application = None):
        self._eo_plan_registry = eo_plan_registry
        self._listen_address = urllib.parse.urlparse("http://" + listen_address)
        self._listen_address_raw = listen_address

        self.app = web.Application(
        middlewares=[
            MonitoringEndpointApp._catch_json_decode_error,
        ])

        self.app.add_routes([
            web.get("/hello", self._hello),
            web.get("/plans", self._get_plans),
            web.get("/plan/latest", self._get_latest_plan),
            web.get("/ui", self._get_ui)
        ])

        if eo_plan_pusher is not None:
            self.app.add_subapp("/eo/", eo_plan_pusher)


    async def start(self):
        log.warning("Starting monitoring api server, listening on %s", self._listen_address_raw)
        runner = web.AppRunner(self.app)
        await runner.setup()
        site = web.TCPSite(runner, host=self._listen_address.hostname, port=self._listen_address.port)
        await site.start()


    @staticmethod
    @web.middleware
    async def _catch_json_decode_error(request, handler):
        try:
            return await handler(request)
        except json.decoder.JSONDecodeError:
            raise web.HTTPBadRequest(body=json.dumps({'message': "Could not decode json body"}))


    async def _hello(self, request):  # pylint: disable=unused-argument
        return web.Response(text="Hello")


    async def _get_plans(self, request):  # pylint: disable=unused-argument
        plans = await self._eo_plan_registry.get_eo_plans_impl()
        json_plans = [EOPlanImplementation.Schema().dump(plan) for plan in plans.values()]
        return json_response(json_plans, status=HTTPStatus.OK)


    async def _get_latest_plan(self, request):  # pylint: disable=unused-argument
        plans = await self._eo_plan_registry.get_eo_plans_impl()
        latest_plan = sorted(plans.values(), key=lambda plan: plan.submission_date, reverse=True)[0] if any(plans) else None
        json_plan = EOPlanImplementation.Schema().dump(latest_plan) if latest_plan else None
        return json_response(json_plan, status=HTTPStatus.OK)


    async def _get_ui(self, request):
        eo_plan_id = request.rel_url.query.get('eo_plan_id')
        all_eo_plans = await self._eo_plan_registry.get_eo_plans_impl()
        all_eo_plan_ids = list(all_eo_plans.keys())
        eo_plan_impl = all_eo_plans.get(eo_plan_id) if eo_plan_id else None
        view_generator = ViewGenerator(eo_plan_impl, all_eo_plan_ids)
        html = view_generator.build_html()
        return web.Response(text=html, content_type='text/html')
