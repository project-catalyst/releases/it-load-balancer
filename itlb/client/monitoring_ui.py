import logging as log
import math

from dataclasses import dataclass
from io import StringIO
from typing import List

from .persist import EOPlanImplementation, ITLPowerAndSLA


@dataclass
class ITLoad:
    cpu: int
    ram: int
    disk: int
    id: str
    orig_dc: str
    placed: bool


class ViewGenerator:
    def __init__(self, plan_impl: EOPlanImplementation, all_eo_plan_ids: List[str]):
        self.plan_impl = plan_impl
        self.all_eo_plan_ids = all_eo_plan_ids
        self._base_itl_square_size_px_per_watt = 10
        self._max_itl_size_px = 250

    def build_html(self):
        sb = StringIO()
        self._write_header(sb)
        self._write_body(sb)

        return sb.getvalue()


    def _write_header(self, sb):
        sb.write('''
            <html lang="en">
            <head>
              <meta charset="utf-8">

              <title>ITLB client visualisation</title>
              <meta name="description" content="Layout test">
              <meta name="author" content="Ashamed Anonymous, please don't git blame">

              <style>''')
        self._write_css(sb)
        sb.write('</style></head>')

    def _write_css(self, sb):

        sb.write(f'''
            body {{
                font-family: sans;
            }}

            .all_body {{
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
            }}

            h1 {{
                text-align: center;
                padding: 30px;
            }}

            a:visited, a {{
                font-size: 2em;
                color: #23b572;
                text-decoration: none;
            }}

            .column {{
                margin: 50px;
            }}

            .eo_plan {{
                width: 25%;
            }}

            .present_itl {{
                width: 25%;
            }}

            .selected_itl {{
                width: 25%;
            }}

            .itl_grid {{
                display: flex;
                flex-wrap: wrap;
            }}

            .itl {{
                border: 1px solid black;
                text-align: center;
                display: flex;
                justify-content: center;
                align-content: center;
                flex-direction: column;
            }}

        ''')



    def _write_body(self, sb):
        sb.write('<body>')
        self._write_top(sb)
        sb.write('<hr />')
        sb.write('<div class="all_body">')
        if self.plan_impl:
            self._write_eo_plan(sb)
            self._write_all_itl(sb)
            self._write_selected_itl(sb)
        sb.write('</div>')

        sb.write('''
            </body>
        </html>''')



    def _write_top(self, sb):
        sb.write('''
                <h1>IT load balancer client visualisation</h1>
                <div id="top">
        ''')

        sb.write('<ul class="tests_links">')
        for eo_plan_id in sorted(self.all_eo_plan_ids):
            if self.plan_impl is not None and eo_plan_id == self.plan_impl.id:
                sb.write(f'<li class="current_test"><a href="/ui?eo_plan_id={eo_plan_id}">Currently visualized: {eo_plan_id}</a></li>')
            else:
                sb.write(f'<li ><a href="/ui?eo_plan_id={eo_plan_id}">{eo_plan_id}</a></li>')
        sb.write('</ul>')
        sb.write('</div>')


    def _write_eo_plan(self, sb):
        sb.write('<div class="eo_plan column">')

        sb.write("<h2>Plan details</h2>")

        sb.write('<ul>')
        sb.write(f'<li>Submission date: {self.plan_impl.submission_date.isoformat()}</li>')

        sb.write(f'<li>Relocate:')
        sb.write('<ul>')
        sb.write(f'<li>start: {self.plan_impl.eo_plan.relocate.start.isoformat()}</li>')
        sb.write(f'<li>end: {self.plan_impl.eo_plan.relocate.end.isoformat()}</li>')
        sb.write(f'<li>energy: {self.plan_impl.eo_plan.relocate.value} kW.h</li>')
        sb.write(f'<li>power: {self.plan_impl.eo_plan.relocate.power_watts} Watt</li>')
        sb.write('</ul>')
        sb.write('</li>')

        sb.write(f'<li>Get:</li>')
        sb.write(f'<li>Shift:</li>')

        sb.write('</div>')


    def _write_all_itl(self, sb):
        sb.write('<div class="present_itl column">')
        sb.write("<h2>Present IT loads</h2>")

        # base_size = 1
        sb.write('<div class="itl_grid">')
        if any(self.plan_impl.all_it_loads):
            biggest_initial_downtime_budget = max((self._initial_downtime_budget_for_itl(ips)
                                                  for ips in self.plan_impl.all_it_loads))
            biggest_power = max((ips.power for ips in self.plan_impl.all_it_loads))
            self._base_itl_square_size_px_per_watt = self._max_itl_size_px / biggest_power
            for ips in sorted(self.plan_impl.all_it_loads, key=self._remaining_downtime_budget_for_itl, reverse=True):
                remaining_downtime = self._remaining_downtime_budget_for_itl(ips)
                red = 1 - remaining_downtime / biggest_initial_downtime_budget
                green = remaining_downtime / biggest_initial_downtime_budget
                blue = 0
                html_color = '#{:02x}{:02x}{:02x}'.format(int(red * 255), int(green * 255), int(blue * 255))

                # sqrt as we want the surface to be proportional to the power in watts
                square_size = self._base_itl_square_size_px_per_watt * math.sqrt(ips.power)
                style = f"width:{square_size}px; max-width: {square_size}px; height:{square_size}px; max-height:{square_size}px; background-color: {html_color};"
                sb.write(f'<div class="itl" style="{style}">{ips.power} W<br />{remaining_downtime} h</div>')

        sb.write('</div>')
        sb.write('</div>')


    def _write_selected_itl(self, sb):
        sb.write('<div class="selected_itl column">')
        sb.write("<h2>IT loads proposed for migration</h2>")

        # base_size = 1
        sb.write('<div class="itl_grid">')
        total_offered_power = 0
        if any(self.plan_impl.all_it_loads):
            biggest_initial_downtime_budget = max(self._initial_downtime_budget_for_itl(ips)
                                                  for ips in self.plan_impl.all_it_loads)
            biggest_power = max(ips.power for ips in self.plan_impl.all_it_loads)
            self._base_itl_square_size_px_per_watt = self._max_itl_size_px / biggest_power

            for proposed_migration in self.plan_impl.migration_request:
                ips = next(ips for ips in self.plan_impl.all_it_loads if ips.itl.vc_tag == proposed_migration.request.vc_tag)
                remaining_downtime = self._remaining_downtime_budget_for_itl(ips)
                red = 1 - remaining_downtime / biggest_initial_downtime_budget
                green = remaining_downtime / biggest_initial_downtime_budget
                blue = 0
                html_color = '#{:02x}{:02x}{:02x}'.format(int(red * 255), int(green * 255), int(blue * 255))

                # sqrt as we want the surface to be proportional to the power in watts
                square_size = self._base_itl_square_size_px_per_watt * math.sqrt(ips.power)
                style = f"width:{square_size}px; max-width: {square_size}px; height:{square_size}px; max-height:{square_size}px; background-color: {html_color};"
                sb.write(f'<div class="itl" style="{style}">{ips.power} W<br />{remaining_downtime} h</div>')
                total_offered_power += ips.power

        sb.write('</div>')
        sb.write(f"<h3>Total offered power: {total_offered_power} Watt")
        sb.write('</div>')


    def _get_downtime_slo_for_itl(self, ips: ITLPowerAndSLA):
        downtime_slo = next((slo for slo in ips.sla.slos
                                  if slo.objective == 'downtime' and slo.status == 'ok'), None)
        return downtime_slo

    def _remaining_downtime_budget_for_itl(self, ips: ITLPowerAndSLA):
        downtime_slo = self._get_downtime_slo_for_itl(ips)
        if not downtime_slo:
            # We can have as much downtime as we want (weird...) let's represent this in a
            # very clumsy way, by a big integer. ¯\_(ツ)_/¯
            log.warning("Found no 'downtime' SLO for %s, this is weird", ips.sla.entity_id)
            return 2**32
        return downtime_slo.target_value - downtime_slo.current_value


    def _initial_downtime_budget_for_itl(self, ips: ITLPowerAndSLA):
        downtime_slo = self._get_downtime_slo_for_itl(ips)
        if not downtime_slo:
            # We can have as much downtime as we want (weird...) let's represent this in a
            # very clumsy way, by a big integer. ¯\_(ツ)_/¯
            log.warning("Found no 'downtime' SLO for %s, this is weird", ips.sla.entity_id)
            return 2**32
        return downtime_slo.target_value
