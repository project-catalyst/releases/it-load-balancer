import aiohttp
import logging as log

from abc import ABC, abstractmethod
from aiohttp import ClientSession
from datetime import datetime, timezone
from marshmallow import Schema as MSchema
from typing import ClassVar, List, Optional, Type, TYPE_CHECKING
from urllib.parse import urljoin

if TYPE_CHECKING:
    from dataclasses import dataclass
else:
    from marshmallow_dataclass import dataclass

from .db_api import ITLoad
from ..common.utils import WithSchema


# {
#     "id": "635228bb-a688-48c7-bda9-90bfb8b3d118",
#     "entity_type": "vc_tag",
#     "entity_id": "0x2cb461696f7c88a214c432fd5fffc29db44614547e847182843e54cea6504d97",
#     "valid_from": "2019-08-05T10:59:44.275731Z",
#     "valid_until": "2019-09-05T10:59:44.275731Z",
#     "status": "active",
#     "slos": [
#         {
#             "id": "eb3dca0b-38e2-4d40-be04-3357c28c05fc",
#             "objective": "downtime",
#             "target_value": 10.0,
#             "current_value": 0.0,
#             "unit": "minutes",
#             "status": "active",
#             "sla": "635228bb-a688-48c7-bda9-90bfb8b3d118",
#             "billing_cycle": "daily",
#             "last_billed": "2019-08-05T11:12:00.376487Z",
#             "next_bill": "2019-08-06T11:12:00.376487Z",
#             "levels": [
#                 {
#                     "id": "371fc802-e365-4c91-880f-95e2b67ecfb2",
#                     "status": "inactive",
#                     "min_value": 10.0,
#                     "max_value": 0.0,
#                     "price_drop_percentage": 100.0,
#                     "slo": "eb3dca0b-38e2-4d40-be04-3357c28c05fc"
#                 },
#                 {
#                     "id": "8cb8e56b-d0cc-45fa-a036-e95cf646fd78",
#                     "status": "active",
#                     "min_value": 0.0,
#                     "max_value": 10.0,
#                     "price_drop_percentage": 0.0,
#                     "slo": "eb3dca0b-38e2-4d40-be04-3357c28c05fc"
#                 }
#             ],
#             "events": [
#                 {
#                     "id": "b56ceba9-41de-470d-a00b-6c1a76f3386a",
#                     "type": "downtime",
#                     "status": "completed",
#                     "start_time": "2019-08-05T11:05:00Z",
#                     "end_time": "2019-08-05T11:10:00Z",
#                     "value": 5.0,
#                     "slo": "eb3dca0b-38e2-4d40-be04-3357c28c05fc"
#                 }
#             ],
#             "history": [
#                 {
#                     "billing_time": "2019-08-05T11:12:00.376487Z",
#                     "start": "2019-08-06T10:59:44.275731Z",
#                     "end": "2019-08-05T11:12:00.376487Z",
#                     "value": 5.0,
#                     "status": "active"
#                 }
#             ]
#         }
#     ]
# }



# NOTE: as a first approximation, we'll forget about the various levels and only use the
#       difference between target_value and current_value, so the following dataclass does
#       not include all the structure of the SLA status as returned by a real SLARC.

@dataclass
class SLO(WithSchema):
    id: str
    objective: str
    target_value: float
    current_value: float
    unit: str
    status: str
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    class Meta:
        unknown = 'exclude'


@dataclass
class SLAStatus(WithSchema):
    id: str
    entity_type: str
    entity_id: str
    valid_from: datetime
    valid_until: datetime
    status: Optional[str]
    slos: List[SLO]
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy

    class Meta:
        unknown = 'exclude'


class SLARCClient(ABC):
    @abstractmethod
    async def get_sla_status_for_itl(self, itl: ITLoad) -> SLAStatus:
        pass


class SLARCClientFake(SLARCClient):
    async def get_sla_status_for_itl(self, itl: ITLoad) -> SLAStatus:
        # For debug purpose, we don't want all the SLA status to be the same, but fot
        # the same goal, it's also quite convenient to have them be the same every time.
        # Let's make this deterministic the last digit of the UUID, IT loads from
        # ITLoadSourceMock conveniently having a different one.
        # Also, we'll only generate the 'downtime' SLO as it's the only one that will
        # be used by upper level anyway.

        slo = SLO(id='00000000-1111-1111-1111-111111111111' + itl.uuid, objective='downtime',
                  # target_value=10, current_value=10 - int(itl.uuid[-1:]), unit='minutes', status='active')
                  target_value=10, current_value=1, unit='minutes', status='active')

        sla_status = SLAStatus(id='00000000-2222-2222-2222-222222222222', entity_type='uuid',
                               entity_id=itl.uuid, valid_from=datetime.now(timezone.utc), valid_until=datetime.now(timezone.utc),
                               status='active', slos=[slo])

        return sla_status



class SLARCClientHttp(SLARCClient):
    def __init__(self, remote_endpoint):
        self._remote_endpoint = remote_endpoint
        # Don't explode the remote host, limit concurrency
        self._tcp_connector = aiohttp.connector.TCPConnector(limit=10)
        self._http_session = ClientSession(connector=self._tcp_connector)

    def _query_uri_for_itl(self, itl):
        return urljoin(urljoin(self._remote_endpoint, 'api/slas/by-type/uuid/'), itl.uuid)

    async def get_sla_status_for_itl(self, itl: ITLoad) -> SLAStatus:
        log.debug("Getting SLA status for %s, with URI %s", itl.uuid, self._query_uri_for_itl(itl))
        response = await self._http_session.get(url=self._query_uri_for_itl(itl))
        json_sla_status = await response.json()

        log.debug("SLA json: %s", json_sla_status)
        sla_status_list = [SLAStatus.Schema().load(item) for item in json_sla_status]
        return sla_status_list[0]
