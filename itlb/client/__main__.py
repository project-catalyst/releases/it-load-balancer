import argparse
import asyncio
import logging as log
import sys
import yaml

from .db_api import (DBApiClient, ITLoadSourceMock, ITLoadSourceFile, ServersSourceMock,
                     ServersSourceFile, VCTagMapperFake, VCTagMapperHttp)
from .dcmc import DCMCClientFake, DCMCClientHttp, DCMCEndpointApp
from .energy_optimiser import (EOPlanSourceHttp, EOPlanSourceFile, EOPlanSourceOneFakeGet,
                               EOPlanSourceOneFakeRelocate, EOPlanSourceHttpTrigger, EOPlanPoller)
from .ite_mapper import ITEMapperFixedByCpu
from .itlbclient import ITLBClientDaemon
from .monitoring_endpoint import MonitoringEndpointApp
from .persist import EOPlanRegistryMemory
from .pricing import PricingPolicyFixed
from .sla import SLARCClientFake, SLARCClientHttp


def make_eo_poller(config, message_queue):
    poller_config = config.get('poller')
    if not poller_config:
        raise RuntimeError("Missing configuration section energy_optimiser.poller")

    source_config = config['source']
    if not source_config:
        raise RuntimeError("Missing configuration section energy_optimiser.source")

    source_type = source_config['type']
    source_subconfig = source_config.get('config') or {}
    if source_type == 'EOPlanSourceOneFakeRelocate':
        source = EOPlanSourceOneFakeRelocate(**source_subconfig)
    elif source_type == 'EOPlanSourceOneFakeGet':
        source = EOPlanSourceOneFakeGet(**source_subconfig)
    elif source_type == 'EOPlanSourceHttpTrigger':
        source = EOPlanSourceHttpTrigger(**source_subconfig)
    elif source_type == 'EOPlanSourceHttp':
        source = EOPlanSourceHttp(**source_subconfig)
    elif source_type == 'EOPlanSourceFile':
        source = EOPlanSourceFile(**source_subconfig)
    else:
        raise RuntimeError("Unhandled EO plan source: {}".format(source_type))

    return EOPlanPoller(dc_id="on-est-pas-dc", eo_plan_source=source, **poller_config, message_queue=message_queue)


def make_vc_tag_mapper(config):
    mapper_type = config.get('type')
    if mapper_type == 'fake':
        return VCTagMapperFake()
    elif mapper_type == 'http':
        subconfig = config.get('config')
        if not subconfig:
            raise RuntimeError("Missing config for http vc_tag mapper")
        return VCTagMapperHttp(**subconfig)


def make_db_api_client(config, vc_tag_mapper):
    client_type = config.get('type')
    if client_type == 'fake':
        it_load_source = ITLoadSourceMock()
        servers_source = ServersSourceMock()
    elif client_type == 'file':
        it_load_source = ITLoadSourceFile(config.get('config').get('itload_path'))
        servers_source = ServersSourceFile(config.get('config').get('servers_path'))
    else:
        raise RuntimeError(f"Unhandled type {client_type} for db-api-client")

    return DBApiClient(it_load_source, servers_source, vc_tag_mapper)


def make_ite_mapper(config):
    policy = config.get('policy', '<no-policy>')
    if policy == 'fixed-by-cpu':
        watts_per_cpu = int(config['config'].get('watts_per_cpu', 10))
        return ITEMapperFixedByCpu(watts_per_cpu)
    else:
        raise ValueError(f"Unhandled ITE mapper type {policy}")

def make_dcmc_client(config):
    client_type = config.get('type')
    if client_type == 'fake':
        return DCMCClientFake()
    elif client_type == 'http':
        subconfig = config.get('config')
        if not subconfig:
            raise RuntimeError("Missing config for http dcmc client")
        return DCMCClientHttp(**subconfig)
    else:
        raise RuntimeError(f"Unknown DCMC client type '{client_type}'")


def make_pricing_policy(config):
    policy = config.get('policy')
    if policy == 'fixed':
        subconfig = config.get('config', {})
        return PricingPolicyFixed(**subconfig)
    else:
        raise RuntimeError(f"Unknown pricing policy '{policy}'")

def make_slarc_client(config):
    client_type = config.get('type')
    if client_type == 'fake':
        return SLARCClientFake()
    elif client_type == 'http':
        subconfig =config.get('config')
        if not subconfig:
            raise RuntimeError("Missing config for http SLARC client")
        return SLARCClientHttp(**subconfig)

def make_monitoring_endpoint(config, eo_plan_registry, eo_plan_pusher):
    monitoring_endpoint = MonitoringEndpointApp(eo_plan_registry=eo_plan_registry,
                                                eo_plan_pusher=eo_plan_pusher, **config)
    return monitoring_endpoint

def make_eo_plan_registry(config):
    registry_type = config.get('type')
    if registry_type == 'memory':
        return EOPlanRegistryMemory()
    else:
        raise RuntimeError(f"Unknown EO plan registry type '{registry_type}'")


async def main():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--config", required=True, help="Configuration file")
    args = args_parser.parse_args()

    try:
        with open(args.config) as config_file:
            config = yaml.safe_load(config_file)
    except:
        log.exception("Cannot parse configuration file %s", args.config)
        sys.exit(1)

    log.basicConfig(level=config['log_level'],
                    format="%(asctime)s|%(process)d(%(processName)s):%(thread)d|%(name)s:%(filename)s:%(lineno)s|%(funcName)s|%(levelname)s|%(message)s",
                    stream=sys.stdout)

    message_queue = asyncio.Queue()

    energy_optimiser_config = config.get('energy-optimiser')
    if not energy_optimiser_config:
        raise RuntimeError("Missing 'energy-optimiser' config")

    eo_poller = make_eo_poller(energy_optimiser_config, message_queue)
    dcmc_endpoint = DCMCEndpointApp(message_queue=message_queue, **config['dcmc-endpoint'])

    dcmc_client_config = config.get('dcmc-client')
    if not dcmc_client_config:
        raise RuntimeError("Missing 'dcmc-client' config")

    dcmc_client = make_dcmc_client(dcmc_client_config)

    shifting_manager = None  # XXX

    vc_tag_mapper_config = config.get('vc-tag-mapper')
    if not vc_tag_mapper_config:
        raise RuntimeError("Missing 'vc-tag-mapper' config")
    vc_tag_mapper = make_vc_tag_mapper(vc_tag_mapper_config)

    db_api_client_config = config.get('db-api-client')
    if not db_api_client_config:
        raise RuntimeError("Missing 'db-api-client' config")
    db_api_client = make_db_api_client(db_api_client_config, vc_tag_mapper)

    ite_mapper_config = config.get('ite-mapper')
    ite_mapper = make_ite_mapper(ite_mapper_config)

    pricing_policy_config = config.get('pricing-policy')
    if not pricing_policy_config:
        raise RuntimeError("Missing 'pricing-policy' config")
    pricing_policy = make_pricing_policy(pricing_policy_config)

    slarc_client_config = config.get('slarc-client')
    if not slarc_client_config:
        raise RuntimeError("Missing 'slarc-client' config")
    slarc_client = make_slarc_client(slarc_client_config)

    eo_plan_registry_config = config.get('eo-plan-registry')
    if not eo_plan_registry_config:
        raise RuntimeError("Missing 'eo-plan-registry' config")
    eo_plan_registry = make_eo_plan_registry(eo_plan_registry_config)

    monitoring_endpoint_config = config.get('monitoring-endpoint')
    if not monitoring_endpoint_config:
        log.warning("Missing 'monitoring-endpoint' config, so none will be started")
        monitoring_endpoint = None
    else:
        eo_plan_pusher = (eo_poller.eo_plan_source.app
                          if isinstance(eo_poller.eo_plan_source, EOPlanSourceHttpTrigger) else None)
        monitoring_endpoint = make_monitoring_endpoint(monitoring_endpoint_config, eo_plan_registry,
                                                       eo_plan_pusher)


    eo_plan_retry_delay_sec = config.get("eo-plan-retry-delay-set", 10)
    eo_plan_max_tries = config.get("eo-plan-max-tries", 10000)

    main_daemon = ITLBClientDaemon(queue=message_queue, eo_poller=eo_poller, dcmc_endpoint=dcmc_endpoint,
                                   dcmc_client=dcmc_client, shifting_manager=shifting_manager,
                                   db_api_client=db_api_client, ite_mapper=ite_mapper,
                                   pricing_policy=pricing_policy, slarc_client=slarc_client,
                                   monitoring_endpoint=monitoring_endpoint, eo_plan_registry=eo_plan_registry,
                                   eo_plan_retry_delay_sec=eo_plan_retry_delay_sec,
                                   eo_plan_max_tries=eo_plan_max_tries)

    await main_daemon.run()



if __name__ == '__main__':
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        log.warning("Exiting as requested")
    except Exception as ex:
        log.exception("Exception in main application, of type %s", type(ex))
