import asyncio
import dataclasses
import copy

from abc import abstractmethod, ABC
from datetime import datetime
from marshmallow import Schema as MSchema
from typing import ClassVar, List, Type, TYPE_CHECKING

if TYPE_CHECKING:
    from dataclasses import dataclass
else:
    from marshmallow_dataclass import dataclass


from .db_api import ITLoad, Server
from .dcmc import MigrationRequestItem
from .energy_optimiser import EOPlan
from .sla import SLAStatus


@dataclass
class ITLPowerAndSLA:
    itl: ITLoad
    power: float
    sla: SLAStatus
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@dataclass
class ServerAndPower:
    server: Server
    power: float
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy


@dataclass
class ProposedMigrationRequestItem:
    request: MigrationRequestItem
    status: str


@dataclass
class EOPlanImplementation:
    ''' Represents an EO plan that is being or has been implemented (or failed to
        be implemented), along with all the required information to track what
        happened (which IT loads or servers were present, what was their SLA, which
        were offered / bid in the end, the result of individual offers / bids, ... '''

    id: str
    submission_date: datetime
    eo_plan: EOPlan
    all_it_loads: List[ITLPowerAndSLA] = dataclasses.field(default_factory=list)
    all_servers: List[ServerAndPower] = dataclasses.field(default_factory=list)
    migration_request: List[ProposedMigrationRequestItem] = dataclasses.field(default_factory=list)
    Schema: ClassVar[Type[MSchema]] = MSchema  # for mypy



class EOPlanRegistry(ABC):
    @abstractmethod
    async def get_eo_plans_impl(self) -> List[EOPlanImplementation]:
        pass

    @abstractmethod
    async def add_or_update_eo_plan_impl(self, eo_plan_impl: EOPlanImplementation):
        pass


class EOPlanRegistryMemory(EOPlanRegistry):
    def __init__(self):
        self._lock = asyncio.Lock()
        self._eo_plans_impl = dict()


    async def get_eo_plans_impl(self) -> List[EOPlanImplementation]:
        async with self._lock:
            # Make a deep copy to prevent funny stuff like modified collections
            # while reading. Hope it's enough.
            return {plan_id: copy.deepcopy(plan) for plan_id, plan in self._eo_plans_impl.items()}

    async def add_or_update_eo_plan_impl(self, eo_plan_impl: EOPlanImplementation):
        async with self._lock:
            self._eo_plans_impl[eo_plan_impl.id] = eo_plan_impl
