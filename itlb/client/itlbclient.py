import asyncio
import logging as log

from datetime import datetime, timedelta, timezone
from typing import List, Tuple

from .db_api import DBApiClient, Server
from .dcmc import DCMCClient, DCMCEndpointApp, LoadValue, MigrationRequestItem, MigrationStatus
from .energy_optimiser import EOPlan, EOPlanPoller, EOPlanStatus
from .ite_mapper import ITEMapper
from .monitoring_endpoint import MonitoringEndpointApp
from .persist import (EOPlanImplementation, EOPlanRegistry, ITLPowerAndSLA, ProposedMigrationRequestItem,
                      ServerAndPower)
from .pricing import PricingPolicy
from .sla import SLARCClient, SLAStatus, SLO

class ITLBClientDaemon:
    def __init__(self, *, queue: asyncio.Queue, eo_poller: EOPlanPoller,
                 dcmc_endpoint: DCMCEndpointApp, dcmc_client: DCMCClient,
                 shifting_manager, db_api_client: DBApiClient, ite_mapper: ITEMapper,
                 pricing_policy: PricingPolicy, slarc_client: SLARCClient,
                 monitoring_endpoint: MonitoringEndpointApp, eo_plan_registry: EOPlanRegistry,
                 eo_plan_retry_delay_sec: int = 10, eo_plan_max_tries: int = 1000):
        self._queue = queue
        self._eo_poller = eo_poller
        self._dcmc_endpoint = dcmc_endpoint
        self._dcmc_client = dcmc_client
        self._shifting_manager = shifting_manager
        self._db_api_client = db_api_client
        self._ite_mapper = ite_mapper
        self._pricing_policy = pricing_policy
        self._slarc_client = slarc_client
        self._monitoring_endpoint = monitoring_endpoint
        self._eo_plan_registry = eo_plan_registry
        self._eo_plan_retry_delay_sec = eo_plan_retry_delay_sec
        self._eo_plan_max_tries = eo_plan_max_tries

        # About this lock: let's be careful of handling one EO plan at a time only, or things will go
        # badly. Note that this should mostly never happen as EO plans are supposed to be published
        # every hour maximum, and handling them (at least the request phase, placing offers / bids)
        # should not take that long, with a big margin. Still, better be on the safe side.
        # NOTE: patterns like trying to cancel a now-outdated plan would be better on paper. Here
        # requests will just pile up endlessly if they come in faster than they are treated, but it
        # seems overly complicated for a case that won't happen.
        self._eo_plan_handling_lock = asyncio.Lock()


    async def run(self):
        # Start sub-components
        asyncio.create_task(self._eo_poller.loop())
        await self._dcmc_endpoint.start()
        if self._monitoring_endpoint:
            await self._monitoring_endpoint.start()

        while True:
            log.info("Waiting for a message...")
            msg = await self._queue.get()
            try:
                if isinstance(msg, DCMCEndpointApp.Hello):
                    log.info("Got an Hello message, yay!")
                elif isinstance(msg, EOPlan):
                    # Handling an EO plan may be relatively long (querying the DB API, then the
                    # SLARC, then talking to the DCMC), so let's execute it concurrently. Most of
                    # the wait time will be IO so just spawning a task should be enough.
                    # asyncio.create_task(self._handle_optimisation_plan(msg))
                    await self._handle_optimisation_plan(msg)
                elif isinstance(msg, list) and isinstance(msg[0], MigrationStatus):
                    log.info("Got migration statuses: %s", msg)
                    await self._handle_migration_status(msg)
                else:
                    log.error("Currently unhandled message of type %s: %s", type(msg), msg)
            except Exception:
                log.exception("Exception while handling message of type %s: ", type(msg))


    async def _handle_migration_status(self, migration_status_list: List[MigrationStatus]):
        all_plans = await self._eo_plan_registry.get_eo_plans_impl()
        plans_to_update = list()
        for migration_status in migration_status_list:
            eo_plan_impl = None
            proposed_migration_request = None
            for plan in all_plans.values():
                p_req = next((p_req for p_req in plan.migration_request
                                    if p_req.request.vc_tag == migration_status.vc_tag), None)
                if p_req:
                    eo_plan_impl = plan
                    proposed_migration_request = p_req
                    break

            if not eo_plan_impl:
                log.error("Got a migration status for VC tag %s, but did not find an EO plan with it",
                          migration_status.vc_tag)
                continue

            proposed_migration_request.status = migration_status.status
            if all(migration_request.status.lower() == "accepted" for migration_request in eo_plan_impl.migration_request):
                eo_plan_impl.eo_plan.status = EOPlanStatus.IMPLEMENTING
            elif any(migration_request.status.lower() == "rejected" for migration_request in eo_plan_impl.migration_request):
                eo_plan_impl.eo_plan.status = EOPlanStatus.REJECTED
            plans_to_update.append(eo_plan_impl)

        for eo_plan in plans_to_update:
            await self._eo_plan_registry.add_or_update_eo_plan_impl(eo_plan)



    # This in its own "greenlet" of execution, so we are allowed to make time consuming
    # async IO, but not long CPU-limited operations. If CPU heavy operations are required,
    # we should move this in a ProcessPoolExecutor (see ITLB server for an example of this).
    async def _handle_optimisation_plan(self, eo_plan: EOPlan) -> None:
        nb_tries = 0
        while True:
            try:
                async with self._eo_plan_handling_lock:
                    log.info("Got a new EOPlan: %s", eo_plan)

                    log.info("Add EOPlan to the registry")
                    eo_plan_impl = EOPlanImplementation(eo_plan.aggregate_id, datetime.now(timezone.utc), eo_plan=eo_plan)
                    await self._eo_plan_registry.add_or_update_eo_plan_impl(eo_plan_impl)

                    if eo_plan.accept is not None and eo_plan.relocate is not None:
                        log.error("EO plan has both 'accept' and 'relocate' actions, which is "
                                  "inconsistent, droping this EO plan")
                        return

                    migration_request = None
                    migration_error = None
                    if eo_plan.accept:
                        migration_request, migration_error = await self._handle_accept_plan(eo_plan_impl)
                    elif eo_plan.relocate:
                        migration_request, migration_error = await self._handle_relocate_plan(eo_plan_impl)
                    else:
                        log.warning("Neither accept nor relocate, that's weird...")

                if migration_error:
                    log.error("Failed to implement EO plan: %s", migration_error)
                    break
                else:
                    await self._dcmc_client.send_migration_request(migration_request)
                    break
            except Exception:
                nb_tries += 1
                if nb_tries < self._eo_plan_max_tries:
                    log.exception("Exception while implementing EO plan, retry in %d seconds. It will be try %d/%d. ",
                                  self._eo_plan_retry_delay_sec, nb_tries + 1, self._eo_plan_max_tries)
                    await asyncio.sleep(self._eo_plan_retry_delay_sec)
                else:
                    log.exception("Exception while implementing EO plan, no more retries left, abandon")
                    break



    async def _to_migration_request(self, start: datetime, end: datetime, servers_or_itl) -> List[MigrationRequestItem]:
        async def to_migration_request_item(server_or_itl):
            cpu = LoadValue('cpu', server_or_itl.vcpu, "cpu")
            ram = LoadValue('ram', server_or_itl.vram, "MB")
            disk = LoadValue('disk', server_or_itl.vdisk, "GB")
            date = datetime.now(timezone.utc)  # FIXME: really?

            price = self._pricing_policy.price_for_it(start, end, server_or_itl.vcpu,
                                                      server_or_itl.vram, server_or_itl.vdisk)

            if isinstance(server_or_itl, Server):
                vc_tag = 'srv-' + server_or_itl.uuid
                action_type = 'bid'
            else:
                vc_tag = server_or_itl.vc_tag
                action_type = 'offer'

            return MigrationRequestItem(date=date, vc_tag=vc_tag, starttime=start, endtime=end,
                                        load_values=[cpu, ram, disk], price=price, action_type=action_type)

        return [await to_migration_request_item(item) for item in servers_or_itl]



    async def _handle_accept_plan(self, eo_plan_impl: EOPlanImplementation) -> List[MigrationRequestItem]:
        # The the goal is to select servers with the capacity to host IT loads amounting
        # to the correct amount of computing power

        eo_plan = eo_plan_impl.eo_plan
        servers_to_bid = []
        needed_watts = eo_plan.accept.power_watts
        remaining_needed_watts = needed_watts

        log.info("To implement 'accept' EO plan, we need to find free servers amounting to %f watts",
                 needed_watts)

        available_servers = await self._db_api_client.get_servers()
        servers_and_powers = [ServerAndPower(server, self._ite_mapper.get_watts_for_it(server.vcpu,
                                                                                       server.vram, server.vdisk))
                              for server in available_servers]

        servers_and_powers.sort(key=lambda srv_pow: srv_pow.power, reverse=True)

        for srv_and_pow in servers_and_powers:
            server = srv_and_pow.server
            power = srv_and_pow.power
            if power <= remaining_needed_watts:
                log.debug("Remaining %f / %f watts to find, take server %s for %f watts",
                          remaining_needed_watts, needed_watts, server, power)
                servers_to_bid.append(server)
                remaining_needed_watts -= power
                if remaining_needed_watts == 0:
                    break
            else:
                log.debug("Remaining %f / %f watts to find, split server %s that has %f watts",
                          remaining_needed_watts, needed_watts, server, power)
                # NOTE: there is an implicit assumption here that we'll have enough resources on this
                # server in all 3 dimensions. That only works currently because ram and disk are ignored
                # for watt counting.
                needed_cpus, _, _ = self._ite_mapper.get_it_for_watts(remaining_needed_watts)
                partial_server = Server(uuid=server.uuid, vcpu=needed_cpus, vram=server.vram, vdisk=server.vdisk)
                remaining_needed_watts = 0
                servers_to_bid.append(partial_server)
                break

        migration_request = await self._to_migration_request(eo_plan.accept.start, eo_plan.accept.end, servers_to_bid)

        eo_plan_impl.all_servers = servers_and_powers
        eo_plan_impl.migration_request = [ProposedMigrationRequestItem(req, status="submitted")
                                          for req in migration_request]
        await self._eo_plan_registry.add_or_update_eo_plan_impl(eo_plan_impl)
        return migration_request



    async def _handle_relocate_plan(self, eo_plan_impl: EOPlanImplementation) -> Tuple[List[MigrationRequestItem], str]:
        log.info("Start handling relocate plan")
        eo_plan = eo_plan_impl.eo_plan

        available_it_loads = await self._db_api_client.get_it_loads()
        available_it_loads = [itl for itl in available_it_loads if not itl.already_gone]
        ips_to_offer = []
        needed_watts = eo_plan.relocate.power_watts
        remaining_needed_watts = needed_watts

        log.info("Got Available IT loads, now getting their power and SLA")

        slas = await asyncio.gather(*[self._slarc_client.get_sla_status_for_itl(itl) for itl in available_it_loads])

        itl_powers_sla = [ITLPowerAndSLA(itl, self._ite_mapper.get_watts_for_it(itl.vcpu, itl.vram, itl.vdisk), sla)
                           for itl, sla in zip(available_it_loads, slas)]

        # What the hell should we do if we cannot find EXACTLY the right amount of watts? We will allow
        # some margin, there is no other sensible solution.
        # NOTE: this greedy approach may fail to find some combination of IT load that would
        # work for the required amount of energy. The problem is a knapsack and could be resolved
        # with better heuristics (though not exactly because of its NP-completeness nature)

        def get_remaining_downtime(sla_status: SLAStatus) -> float:
            downtime_slo: SLO = next((slo for slo in sla_status.slos
                                      if slo.objective == 'downtime' and slo.status == 'active'), None)
            if not downtime_slo:
                # We can have as much downtime as we want (weird...) let's represent this in a
                # very clumsy way, by a big integer. ¯\_(ツ)_/¯
                log.warning("Found no 'downtime' SLO for %s, this is weird", sla_status.entity_id)
                return 2**32
            return downtime_slo.target_value - downtime_slo.current_value

        itl_powers_sla.sort(key=lambda ips: (get_remaining_downtime(ips.sla), 1/ips.power), reverse=True)

        for ips in itl_powers_sla:
            itl = ips.itl
            power = ips.power
            log.debug("Remaining %f / %f watts to find, take ITL %s for %f watts",
                      remaining_needed_watts, needed_watts, itl, power)
            ips_to_offer.append(ips)
            remaining_needed_watts -= power
            if remaining_needed_watts <= 0:
                break

        if remaining_needed_watts > 0:
            # We didn't find enough ITL to migrate, we must directly inform the EO that the
            # plan cannot be implemented.
            eo_plan_impl.eo_plan.status = EOPlanStatus.REJECTED
            return None, "Not enough IT loads"
        else:
            # We have enough. Because of the greedy choice chosed, we may have more in a ridiculous way
            # that can easily be avoided. Let's try to avoid some simple cases.
            # Once again, the real solution is a knapsack.
            excedent = -remaining_needed_watts
            to_remove = []
            for ips in sorted(ips_to_offer, key=lambda ips: ips.power):
                if ips.power <= excedent:
                    to_remove.append(ips)
                    excedent -= ips.power
            for ips in to_remove:
                ips_to_offer.remove(ips)

            move_start_time = eo_plan.relocate.start + timedelta(milliseconds=eo_plan.relocate.move_from_date)
            migration_request = await self._to_migration_request(move_start_time, eo_plan.relocate.end,
                                                                 [ips.itl for ips in ips_to_offer])
            # Mark ITLs as migrated, so no longer there
            for ips in ips_to_offer:
                ips.itl.already_gone = True

            eo_plan_impl.all_it_loads = itl_powers_sla
            eo_plan_impl.migration_request = [ProposedMigrationRequestItem(req, status='submitted')
                                              for req in migration_request]

        await self._eo_plan_registry.add_or_update_eo_plan_impl(eo_plan_impl)
        return migration_request, None
