import aiohttp
import asyncio
import json
import logging as log

from abc import ABC, abstractmethod
from aiohttp import ClientSession
from http import HTTPStatus
from marshmallow import Schema
from marshmallow_dataclass import dataclass
from typing import ClassVar, List, Optional, Type
from urllib.parse import urljoin



# NOTE: not sure about the data format that the monitoring DB API

@dataclass
class ITLoad:
    uuid: str
    vc_type: str
    vcpu: int
    vram: int
    vdisk: int
    vc_tag: Optional[str] = None
    already_gone: bool = False
    Schema: ClassVar[Type[Schema]] = Schema  # for mypy




@dataclass
class Server:
    uuid: str
    vcpu: int
    vdisk: int
    vram: int
    Schema: ClassVar[Type[Schema]] = Schema  # for mypy



class ITLoadSource(ABC):
    @abstractmethod
    async def get_it_loads(self) -> List[ITLoad]:
        pass


class ITLoadSourceMock(ITLoadSource):
    async def get_it_loads(self):
        return [
            ITLoad(uuid="11111111-1111-1111-1111-111111111111", vc_type="openstack:vm",
                   vcpu=1, vdisk=120, vram=40960),
            ITLoad(uuid="11111111-2222-2222-2222-222222222222", vc_type="openstack:vm",
                   vcpu=2, vdisk=40, vram=8292),
            ITLoad(uuid="11111111-3333-3333-3333-333333333333", vc_type="openstack:vm",
                   vcpu=2, vdisk=40, vram=8392),
            ITLoad(uuid="11111111-4444-4444-4444-444444444444", vc_type="openstack:vm",
                   vcpu=2, vdisk=40, vram=8492),
            ITLoad(uuid="11111111-5555-5555-5555-555555555555", vc_type="openstack:vm",
                   vcpu=4, vdisk=40, vram=8592),
            ITLoad(uuid="11111111-6666-6666-6666-666666666666", vc_type="openstack:vm",
                   vcpu=6, vdisk=40, vram=8692),
        ]


class ITLoadSourceFile(ITLoadSource):
    def __init__(self, path):
        self._path = path
        self._it_loads = None

    @dataclass
    class ITLoadFromFile:
        uuid: str
        cpu: int
        disk: int
        ram: int
        Schema: ClassVar[Type[Schema]] = Schema  # for mypy

        def to_itl(self):
            return ITLoad(uuid=self.uuid, vc_type='openstack:vm', vcpu=self.cpu, vram=self.ram,
                          vdisk=self.disk)

    async def get_it_loads(self):
        if not self._it_loads:
            with open(self._path) as itl_file:
                raw_json = json.load(itl_file)
                self._it_loads = [self.ITLoadFromFile.Schema().load(item).to_itl() for item in raw_json]

        return self._it_loads



class ITLoadSourceHttp(ITLoadSource):
    def __init__(self, uri):
        self._uri = uri
        self._http_session = aiohttp.ClientSession()


    @dataclass
    class ITLoadFromHttp:
        uuid: str
        cpu: int
        disk: int
        ram: int
        Schema: ClassVar[Type[Schema]] = Schema  # for mypy

        def to_itl(self):
            return ITLoad(uuid=self.uuid, vc_type='openstack:vm', vcpu=self.cpu, vram=self.ram,
                          vdisk=self.disk)

    async def get_it_loads(self):
        resp = await self._http_session.get(self._uri)
        raw_itl = resp.json()
        itl_from_http = [self.ITLoadFromHttp.Schema().load(raw_itl) for raw_itl in raw_itl]
        itl = [itl.to_itl() for itl in itl_from_http]
        return itl



class ServersSource(ABC):
    @abstractmethod
    async def get_servers(self) -> List[Server]:
        pass


class ServersSourceMock(ServersSource):
    async def get_servers(self):
        return [
            Server(uuid="AAAAAAAA-1111-1111-1111-111111111111",
                   vcpu=8, vdisk=500, vram=32768),
            Server(uuid="AAAAAAAA-2222-2222-2222-222222222222",
                   vcpu=8, vdisk=500, vram=32768),
            Server(uuid="AAAAAAAA-3333-3333-3333-333333333333",
                   vcpu=4, vdisk=300, vram=20000),
            Server(uuid="AAAAAAAA-4444-4444-4444-444444444444",
                   vcpu=2, vdisk=500, vram=4096),
            Server(uuid="AAAAAAAA-5555-5555-5555-555555555555",
                   vcpu=2, vdisk=500, vram=6000),
        ]


class ServersSourceFile(ServersSource):
    def __init__(self, path):
        self._path = path

    @dataclass
    class ServerFromFile:
        uuid: str
        cpu: int
        disk: int
        ram: int
        Schema: ClassVar[Type[Schema]] = Schema  # for mypy

        def to_server(self):
            return Server(uuid=self.uuid, vcpu=self.cpu, vram=self.ram, vdisk=self.disk)

    async def get_servers(self):
        with open(self._path) as servers_file:
            raw_json = json.load(servers_file)
            return [self.ServerFromFile.Schema().load(item).to_itl() for item in raw_json]



class ServersSourceHttp(ServersSource):
    def __init__(self, uri):
        self._uri = uri
        self._http_session = aiohttp.ClientSession()


    @dataclass
    class ServerFromHttp:
        uuid: str
        cpu: int
        disk: int
        ram: int
        Schema: ClassVar[Type[Schema]] = Schema  # for mypy

        def to_server(self):
            return Server(uuid=self.uuid, vcpu=self.cpu, vram=self.ram, vdisk=self.disk)


    async def get_servers(self):
        resp = await self._http_session.get(self._uri)
        raw_servers = resp.json()
        servers_from_http = [self.ServerFromHttp.Schema().load(raw_server) for raw_server in raw_servers]
        servers = [server.to_server() for server in servers_from_http]
        return servers



class DBApiClient:
    def __init__(self, it_load_source, servers_source, vc_tag_mapper):
        self._it_load_source = it_load_source
        self._servers_source = servers_source
        self._vc_tag_mapper = vc_tag_mapper


    async def get_it_loads(self) -> List[ITLoad]:
        # Get them  raw from the source and decorate them with their VC tag
        it_loads = await self._it_load_source.get_it_loads()
        # Make http requests in parallel
        vc_tags_task = asyncio.gather(*[self._vc_tag_mapper.vc_tag_for_itl(itl) for itl in it_loads])
        try:
            vc_tags = await vc_tags_task
        except Exception:
            vc_tags_task.cancel()
            raise

        for itl, vc_tag in zip(it_loads, vc_tags):
            if not vc_tag:
                log.info("Couldn't find a VC tag for %s, not listing it", itl.uuid)
            else:
                itl.vc_tag = vc_tag

        return [itl for itl in it_loads if itl.vc_tag is not None]

    def get_servers(self):
        return self._servers_source.get_servers()



class VCTagMapper(ABC):
    @abstractmethod
    async def vc_tag_for_itl(self, itl: ITLoad):
        pass


class VCTagMapperFake(VCTagMapper):
    async def vc_tag_for_itl(self, itl: ITLoad) -> str:
        return itl.uuid


class VCTagMapperHttp(VCTagMapper):
    def __init__(self, remote_endpoint: str):
        self._remote_endpoint = remote_endpoint
        # Don't explode the remote host, limit concurrency
        self._tcp_connector = aiohttp.connector.TCPConnector(limit=10)
        self._http_session = ClientSession(connector=self._tcp_connector)

    def _query_uri(self, itl):
        return urljoin(urljoin(self._remote_endpoint, 'api/container/by-id/'), itl.uuid)

    async def vc_tag_for_itl(self, itl: ITLoad) -> str:
        try:
            log.debug("Getting VC tag for %s with uri %s", itl.uuid, self._query_uri(itl))
            response = await self._http_session.get(url=self._query_uri(itl))
            if response.status == HTTPStatus.NOT_FOUND:
                log.debug("There is no VC tag for %s", itl.uuid)
                return None

            response.raise_for_status()

            # Response is as follows, with only one possible tag in the array
            # {
            #     "vc_tags": [ "0x8974ab343v2...444df6" ]
            # }
            resp_json = await response.json()
            tag = resp_json['vc_tags'][0]
            log.debug("VC tag for %s is %s", itl.uuid, tag)
            return tag
        except Exception:
            log.exception("Failed to get VC tag for %s: ", itl.uuid)
            raise
