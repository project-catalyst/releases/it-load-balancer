Energy aware IT load balancer, WP3, task T3.3, deliverable D3.2

# General architecture and components

## Client

Running in each DC, the client is responsible for:

- polling for the optimisation plans from the "Intra DC enery optimiser", that will request 3 types of action:
    + **temporally shift** load (e.g: pause 10% of the IT load and execute them only in two hours)
    + **relocate** a given percentage (or absolute power?) of IT loads inside the DC, i.e consolidating the
      servers, by colocating as many ITL possible on as few physical servers as possible, in order to be
      able to turn off servers or even entire rooms
    + **migrate** IT loads (a given percentage or an absolute power?) to some other DC (the optimisation plan
      does not decide which IT loads and where they are to be migrated)
- implementing those requests:
    + for temporal shifting and relocation: directy talking to the local cloud management platform (CMP)
      to migrate or pause IT loads (typically Openstack management) and to the hosting hardware that can
      be turned on/off (typically using IPMI, or is there some component of Openstack that also handles this?)
    + for migration: select IT loads to migrate, communicate with the LB server that will aggregate requests
      from several DCs, place offers on an IT load marketplace, and perform global optimisation.



## Server

The server part is there to handle cross-DC migrations. It is called as the clearing mechanism of the IT load marketplace.

See Catalyst deliverable D3.2 for more details.



# Run locally / development


## Environment

Both components depend in Python >= 3.7, so you need a compatible version available in your environment.

## Setup dependencies

Both components depend on external Python libraries listed in `itlb/requirements.txt`o

**NOTE**: at the time of writing this, we use pre-releases of `marshmallow` and `marsmallow-dataclass`,
specifically `marshmallow==3.0.0rc7` and `marshmallow-dataclass==6.0.0rc4`. With RC versions
advancing and when final release happens, these static versions may have to be revised in order
for the dependencies install correctly.

Install dependencies with pip (other Python package management may work but have not been tested):

```
python3.7 -m venv venv
source venv/bin/activate
pip install -U -r itlb/requirements.txt
```

## Server

### Configuration

The server awaits a single configuration file, you can find samples in `etc/server`

### Run

You can then run the server from toplevel with:

```
python -m itlb.server --config /path/to/config/file
```

for instance

```
python -m itlb.server --config etc/server/debug.conf
```


## Client

### Configuration

The client awaits a single configuration file, you can find samples in `etc/client`

### Run

You can then run the client from toplevel with:

```
python -m itlb.client --config /path/to/config/file
```

for instance

```
python -m itlb.client --config etc/client/debug.conf
```


# Build and run Docker images

You can build a Docker image for the ITLB server with the following command:

```
docker build -f build/Dockerfile.server -t itlb-server .
```

and you can then run it with, for instance using config in `etc/server/demo.conf` and
binding listening TCP port 6000 in the container (in the config) to port TCP 6000 in
the host, listening locally:

```
docker run --rm -p 127.0.0.1:6000:6000 -v $(readlink -f etc/server/demo-docker.conf):/etc/itlb/itlb.conf itlb-server
```


You can build a Docker image for the ITLB client with the following command:

```
docker build -f build/Dockerfile.client -t itlb-client .
```

and you can then run it with, for instance using config in `etc/client/demo.conf.sample` and
binding listening TCP port 6001 in the container (in the config) to port TCP 6001 (same for 6003) in
the host, listening locally:

```
docker run --rm -p 127.0.0.1:6001:6001 -p 127.0.0.1:6003:6003 -v $(readlink -f etc/client/demo-docker.conf.sample):/etc/itlb/itlb.conf itlb-client
```


> **NOTE about listening address**
> When run inside Docker, the service has to listen on `0.0.0.0` in order for the port forward
> to work correctly. If listening on `localhost` in the container namespace, no forward can be
> performed, so the listening address in the configuration file must specify `0.0.0.0:$PORT`
> where the choice of `$PORT` is free.
> Running with `docker --net host` is an alternative (but discouraged) possibility.
